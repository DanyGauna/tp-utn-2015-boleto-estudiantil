﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="indexEstudiante.aspx.cs" Inherits="TPractico2015.indexEstudiante" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
   
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link href="CSS/bootstrap.min.css" rel="stylesheet" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="assetMasterPage/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="assetMasterPage/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="assetMasterPage/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="assetMasterPage/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="assetMasterPage/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="assetMasterPage/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="assetMasterPage/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="assetMasterPage/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <form id="form1" runat="server">
   
        
     <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>BUS</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Admin</b><strong>BUS</strong></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
             
             
             
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="assetMasterPage/dist/img/avatar5.png" class="user-image" alt="User Image">
                 
                    <asp:Label ID="LblNombreUsuario" runat="server" Text="" class="hidden-xs"></asp:Label>
                   
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="assetMasterPage/dist/img/avatar5.png" class="img-circle" alt="User Image">
                    <p>
                        <asp:Label ID="LblNombreUser" runat="server" Text=""></asp:Label>  
                        <small><asp:Label ID="LblActivo" runat="server" Text="Activo desde --"></asp:Label></small>
                      
                    </p>
                  </li>
                  <!-- Menu Body -->
                  
                  <!-- Menu Footer-->
                  <li class="user-footer">                    
                    <div class="pull-right">
                      <a href="Login.aspx" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="assetMasterPage/dist/img/avatar5.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
               <p><asp:Label ID="LblNombreUsuarioSider" runat="server" Text=""></asp:Label></p>
              <a><i class="fa fa-circle text-success"></i>
                  <asp:Label ID="LblEstado" runat="server" Text=""></asp:Label> 

              </a>
            </div>
          </div>
          <ul class="sidebar-menu">
            <li class="header">Navegador Principal</li>
                      
            <li class="treeview">
              <a>
                <i class="fa fa-users"></i>               
                  <asp:Label ID="LblUsuarioSider" runat="server" Text="Mis Tickets"></asp:Label>
                                          
              </a>              
            </li>  
                 
        <!-- /.sidebar -->
          </ul>

      </aside>

      <!-- Content Wrapper. Contains page content -->

      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Inicio           
          </h1>         
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><asp:Label ID="LblCantidadEstudiantes" runat="server" Text="-"></asp:Label></h3>
                  <p>Estudiantes</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-people"></i>
                </div>
                <a class="small-box-footer">ADminBUS</a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                   <h3> <asp:Label ID="Label1" runat="server" Text="-"></asp:Label></h3>
                  <p>Pasajes Emitidos</p>
                </div>
                <div class="icon">
                  <i class="fa fa-ticket"></i>
                </div>
                <a href="#" class="small-box-footer">ADminBUS</a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><asp:Label ID="LblLineasAdd" runat="server" Text="-"></asp:Label></h3>
                  <p>Líneas Adheridas</p>
                </div>
                <div class="icon">
                  <i class="fa fa-bus"></i>
                </div>
                <a href="#" class="small-box-footer">ADminBUS</a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><asp:Label ID="LblCantidadUsuarios" runat="server" Text="-"></asp:Label></h3>
                  <p>Usuarios</p>
                </div>
                <div class="icon">
                  <i class="fa fa-user"></i>
                </div>
                <a href="#" class="small-box-footer">ADminBUS</a>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->
            <div class="containt">
                <div class="col-lg-12">
                   <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Lista de boletos</h3>
                </div><!-- /.box-header -->
                  <div class="table-responsive">
                        <asp:GridView ID="gvTicketsEstudiantes" runat="server" class="table table-bordered"></asp:GridView>
                  </div>
                 
              </div><!-- /.box --> 
              </div><!-- /.box -->
                  
               
                

            </div>
         
    </div><!-- ./wrapper -->
         
    </div><!-- ./wrapper -->
      

    <!-- jQuery 2.1.4 -->
    <script src="assetMasterPage/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="assetMasterPage/bootstrap/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="assetMasterPage/plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="assetMasterPage/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="assetMasterPage/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="assetMasterPage/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="assetMasterPage/plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="assetMasterPage/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="assetMasterPage/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="assetMasterPage/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="assetMasterPage/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="assetMasterPage/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="assetMasterPage/dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="assetMasterPage/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="assetMasterPage/dist/js/demo.js"></script>

    
    </form>
</body>
</html>

