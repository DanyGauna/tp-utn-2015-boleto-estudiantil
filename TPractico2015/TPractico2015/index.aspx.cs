﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Controladoras;
using Modelos;

namespace TPractico2015
{
    public partial class index : System.Web.UI.Page
    {
        CtrlEstudiantes ctrlEstudiantes = new CtrlEstudiantes();
        CtrlUsuarios ctrlUsuarios = new CtrlUsuarios();
        CtrlTransportesUrbanos ctrlTransUrbanos = new CtrlTransportesUrbanos();
        CtrlInstituciones ctrlInstituciones = new CtrlInstituciones();
        CtrlNivelEducativo ctrlNivelesEducativos = new CtrlNivelEducativo();
        CtrlUrbanTicket ctrlUrbanTicket = new CtrlUrbanTicket();
        Usuario usuario;
        List<InstitucionEducativa> listaInstitucion;
        CtrlUtils ctrlUtils = new CtrlUtils();

        protected void Page_Load(object sender, EventArgs e)
        {
            usuario = (Usuario)Session["currentUser"];
            if (usuario.Correo == null)
            {
                Response.Redirect("Login.aspx");
            }           
            Session["listaInstitutos"] = ctrlInstituciones.getAll();             
            Session["listaNivelesEducativos"] = ctrlNivelesEducativos.getAll();
            Session["listaNivelesEducativosString"] = ctrlNivelesEducativos.obtenerNivelString();           
            Session["listaInstitutosString"] = ctrlInstituciones.obtenerInstitucionesString();
            Session["listaEstudiantes"] = ctrlEstudiantes.getAll();
            Session["listaLocalidadesString"] = ctrlUtils.obtenerLocalidades();
            Session["listaProvinciasString"] = ctrlUtils.obtenerProvincias();
            if (usuario.NivelLogin.Nivel == "Administrador")
            {
                Session["listaEstudiantes"] = ctrlEstudiantes.getAll();
            }
            else
            {
                Session["listaEstudiantes"] = ctrlEstudiantes.getAllPorInstitucion(usuario.Institucion.Id);
            }
           
            Session["listaTransUrbano"] = ctrlTransUrbanos.getAll();
          
            LblCantidadEstudiantes.Text = Convert.ToString(ctrlEstudiantes.cantTotalEstudiantes());
            LblCantidadUsuarios.Text = Convert.ToString(ctrlUsuarios.cantTotalUsuarios());
            LblLineasAdd.Text = Convert.ToString(ctrlTransUrbanos.cantTotalTransportes());
            LblPasajesEmitidos.Text = Convert.ToString(ctrlUrbanTicket.devuelveCantidad());
        }
    }
}