﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Controladoras;
using Modelos;

namespace TPractico2015
{
    public partial class formEditarEncargado : System.Web.UI.Page
    {
        CtrlUsuarios ctrlUsuarios = new CtrlUsuarios();
        CtrlInstituciones ctrlInstituciones = new CtrlInstituciones();
        CtrlNivelesLogin ctrlLogin = new CtrlNivelesLogin();
        Usuario usuario;
        protected void Page_Load(object sender, EventArgs e)
        {
            usuario = (Usuario)Session["currentUser"];
            if (usuario.Correo == null)
            {
                Response.Redirect("Login.aspx");
            }
            else if (usuario.NivelLogin.Nivel != "Administrador")
            {
                Response.Redirect("accesoDenegado.aspx");

            }            
            gvEncargadosEdit.DataSource = ctrlUsuarios.getAll();
            gvEncargadosEdit.DataBind();
        }

        protected void BtnBuscarEncargado_Click(object sender, EventArgs e)
        {
            try
            {
                if (TextBuscarEditEncargado.Text != "")
                {
                    usuario = ctrlUsuarios.buscarPorDocumento(TextBuscarEditEncargado.Text);
                    if (usuario != null)
                    {                      
                        llenarCampos(usuario);
                    }
                }
            }
            catch (Exception ex)
            {
                LblErrorAddEncargadoEdit.Text = ex.Message;
            }
        }

        private void llenarCampos(Usuario usu)
        {
            TextBuscarEditEncargado.Text = "";
            TextId.Text = Convert.ToString(usu.Id);
            TextApellidoEditEncargado.Text = usu.Apellido;
            TextNombreEditEncargado.Text = usu.Nombre;
            TextDocumentoEdit.Text = usu.Documento;
            TextId.Enabled = false;
            TextApellidoEditEncargado.Enabled = false;
            TextNombreEditEncargado.Enabled = false;
            TextDocumentoEdit.Enabled = false;
        }

        protected void BtnAgregarEncargadoEdit_Click(object sender, EventArgs e)
        {

        }

        protected void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (TextId.Text != "")
                {
                    usuario = ctrlUsuarios.buscarPorDocumento(TextDocumentoEdit.Text);
                    if (usuario != null)
                    {
                        ctrlUsuarios.delete(usuario);
                        limpiarCampos();
                        gvEncargadosEdit.DataSource = ctrlUsuarios.getAll();
                        gvEncargadosEdit.DataBind();
                    }
                    else
                    {
                        LblErrorAddEncargadoEdit.Text = "No se pudo eliminar al usuario.";
                    }
                       
                }
                else 
                {
                    LblErrorAddEncargadoEdit.Text = "Debe completar los campos.";
                }
            }
            catch (Exception ex)
            {
                LblErrorAddEncargadoEdit.Text = ex.Message;
            }
        }

        protected void BtnLimpiar_Click(object sender, EventArgs e)
        {

        }

        private void limpiarCampos()
        {
            TextBuscarEditEncargado.Text = "";
            TextApellidoEditEncargado.Text = "";
            TextNombreEditEncargado.Text = "";
            TextDocumentoEdit.Text = "";
        }
    }
}