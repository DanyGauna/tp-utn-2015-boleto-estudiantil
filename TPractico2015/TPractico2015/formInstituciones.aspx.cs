﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Controladoras;

namespace TPractico2015
{
    public partial class formInstituciones : System.Web.UI.Page
    {
        CtrlNivelEducativo ctrlNivelEducativo = new CtrlNivelEducativo();
        CtrlInstituciones ctrlInsitutiones = new CtrlInstituciones();
        List<NivelEducativo> listaNiveles = new List<NivelEducativo>();
        Usuario usuario;

        protected void Page_Load(object sender, EventArgs e)
        {
            usuario = (Usuario)Session["currentUser"];
            if (usuario.Correo == null)
            {
                Response.Redirect("Login.aspx");
            }
            else if (usuario.NivelLogin.Nivel != "Administrador")
            {
                Response.Redirect("accesoDenegado.aspx");

            }
            RepeaterNivelesEducativos.DataSource = ctrlNivelEducativo.obtenerNivel();
            if (!IsPostBack)
            {
                RepeaterNivelesEducativos.DataBind();
            }

            gvNivelesEducativos.DataSource = (List<InstitucionEducativa>)Session["listaInstitutos"];
            gvNivelesEducativos.DataBind();
          
            /*TextProvinciasIntitucion.DataSource = ctrlInsitutiones.obtenerProvincias();
            TextProvinciasIntitucion.DataBind();
            TextCiudadInstitucionDrop.DataSource = ctrlInsitutiones.obtenerLocalidad(TextProvinciasIntitucion.SelectedItem.Text);
            TextCiudadInstitucionDrop.DataBind();*/
        }

        protected void TextProvinciasIntitucion_TextChanged(object sender, EventArgs e)
        {
             Page_Load(sender, e);
        }

        protected List<NivelEducativo> ObtenerLabeldeRepeaterNivelesEducativos()
        {
            RepeaterItemCollection replist = RepeaterNivelesEducativos.Items;
            List<NivelEducativo> listNivelesAux = new List<NivelEducativo>();
            foreach (RepeaterItem r in replist)
            {               

                CheckBox ch = (CheckBox)r.FindControl("CkNivelEducativo");

                if (ch.Checked)
                { 
                    string texto = ((Label)r.FindControl("LblNivelEducativo")).Text;
                    NivelEducativo nv = ctrlNivelEducativo.buscarPorNivel(texto);
                    listNivelesAux.Add(nv);                   
                    
                }
            }
            return listNivelesAux;
            
        }

        protected void BtnAgregarInstitucion_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    listaNiveles = ObtenerLabeldeRepeaterNivelesEducativos();
                    if (listaNiveles.Count > 0)
                    {
                        ctrlInsitutiones.save(TextNombreInstituciones.Text, listaNiveles);
                        limpiarCampos();
                        gvNivelesEducativos.DataSource = ctrlInsitutiones.getAll();
                        gvNivelesEducativos.DataBind();
                    }
                    else
                    {
                        LblErrorInstitucion.Text = "Debe seleccionar al menos un nivel";
                    }
                }
                else
                {
                    LblErrorInstitucion.Text = "Debe completar el nombre";
                }
               
            }
            catch (Exception ex)
            {
                LblErrorInstitucion.Text = ex.Message;
            }


            
        }

        private void limpiarCampos()
        {
            TextNombreInstituciones.Text = "";
            checkFalse();

        }

        private void checkFalse()
        {
            RepeaterItemCollection replist = RepeaterNivelesEducativos.Items;
            List<NivelEducativo> listNivelesAux = new List<NivelEducativo>();
            foreach (RepeaterItem r in replist)
            {

                CheckBox ch = (CheckBox)r.FindControl("CkNivelEducativo");

                if (ch.Checked)
                {
                    ch.Checked = false;

                }
            }
            
        }

        private bool validaCampos()
        {
            bool rta = true;
            if (TextNombreInstituciones.Text == "")
            {
                rta = false;
            }
            return rta;
        }
    }
}