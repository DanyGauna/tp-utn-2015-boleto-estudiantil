﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;

namespace TPractico2015
{
    public partial class masterPage : System.Web.UI.MasterPage
    {
        Usuario usuario;

        protected void Page_Load(object sender, EventArgs e)
        {
            usuario = (Usuario)Session["currentUser"];
            LblNombreUsuario.Text = usuario.Nombre + " " + usuario.Apellido;
            LblNombreUser.Text = usuario.Nombre + " " + usuario.Apellido;
            LblNombreUsuarioSider.Text = usuario.Nombre + " " + usuario.Apellido;                
        }
    }
}