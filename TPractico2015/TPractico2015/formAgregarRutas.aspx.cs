﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Controladoras;
using Modelos;

namespace TPractico2015
{
    public partial class formAgregarRutas : System.Web.UI.Page
    {
        CtrlUtils ctrlUtils = new CtrlUtils();
        CtrlRutas ctrlRutas = new CtrlRutas();
        Usuario usuario;
        protected void Page_Load(object sender, EventArgs e)
        {
            usuario = (Usuario)Session["currentUser"];
            if (usuario.Correo == null)
            {
                Response.Redirect("Login.aspx");
            }
            TextLocalidadOrigen.DataSource = Session["listaLocalidadesString"];
            TextLocalidadDestino.DataSource = Session["listaLocalidadesString"];
            if (!IsPostBack)
            {
                TextLocalidadOrigen.DataBind();
                TextLocalidadDestino.DataBind();
                TextCantDias.Text = "1";
            }
            TextHorarioSalida.Text = DateTime.Now.ToShortTimeString().ToString();
            
        }

        protected void BtnAgregarRuta_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    int cantDias = Convert.ToInt32(TextCantDias.Text);
                   
                    if (cantDias > 0)
                    {
                        DateTime hora = Convert.ToDateTime(TextHorarioSalida.Text);
                        ctrlRutas.save(TextLocalidadOrigen.SelectedItem.Text, TextLocalidadDestino.SelectedItem.Text, cantDias, hora);

                    }
                    else
                    {
                        LblErrorAddRuta.Text = "Debe ingresar una cantidad de dias válida.";
                    }
                }
                else
                {
                    LblErrorAddRuta.Text = "Debe completar todos los campos"; 
                }
                
            }
            catch (Exception ex)
            {
                LblErrorAddRuta.Text = ex.Message;
            }
        }

        private bool validaCampos()
        {
            bool rta = true;

            if (TextCantDias.Text == "")
            {
                rta = false;
            }
            else if (TextHorarioSalida.Text == "")
            {
                rta = false; 
            }

            return rta;
        }
    }
}