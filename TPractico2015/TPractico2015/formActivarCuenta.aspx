﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="formActivarCuenta.aspx.cs" Inherits="TPractico2015.formActivarCuenta" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!--<link href="CSS/index.css" rel="stylesheet" />
    <link href="CSS/bootstrap.css" rel="stylesheet" />-->
   
    <title>Registrarse</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assetLogin/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assetLogin/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assetLogin/assets/css/form-elements.css">
        <link rel="stylesheet" href="assetLogin/assets/css/style.css">
        <link href="CSS/inicio.css" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assetLogin/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assetLogin/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assetLogin/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assetLogin/assets/ico/apple-touch-icon-57-precomposed.png">

</head>
<body>
    <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1>Ámbito Estudiantil <a class="alert-link">Registrarse</a> </h1>
                            <!--<div class="description">
                            	<p>
                                    Gestor hotelero funcional que le permite controlar su empresa en todo momento.		                            	
                            	</p>
                            </div>-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3> <a class="alert-link" href="Login.aspx">Login</a> | Registrarse</h3>
                            		<p>Complete todos los campos:</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-pencil"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form id="Form1" role="form" action="" method="post" class="login-form" runat="server">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Nombre</label>
			                        	
                                        <asp:TextBox ID="TextNombreRegistro"  placeholder="Nombre..." class="form-username form-control" runat="server" ToolTip="Nombre"></asp:TextBox>
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Apellido</label>
			                        	
                                        <asp:TextBox ID="TextApellidoRegistro" name="form-password" placeholder="Apellido..." class="form-password form-control"  runat="server" ToolTip="Apellido"></asp:TextBox>
			                        </div>
                                    <div class="form-group">
			                        	<label class="sr-only" for="form-fechaNacimiento">Fecha de Nacimiento</label>
			                        	
                                        <asp:TextBox ID="TextFechaNacimientoRegistro" name="form-password" placeholder="Fecha de Nacimiento..." class="form-password form-control"  runat="server" ToolTip="Fecha de Nacimiento"></asp:TextBox>
			                        </div>
                                    <div class="form-group">
			                        	<label class="sr-only" for="form-password">Tipo de Documento</label>
			                        	
                                        <asp:TextBox ID="TextTipoDocRegistro" name="form-password" placeholder="Tipo de Documento..." class="form-password form-control"  runat="server" ToolTip="Tipo de Documento"></asp:TextBox>
			                        </div>
                                     <div class="form-group">
			                        	<label class="sr-only" for="form-password">Número de Documento</label>
			                        	
                                        <asp:TextBox ID="TextNumDocRegistro" name="form-password" placeholder="Número de Documento..." class="form-password form-control"  runat="server" ToolTip="Número de Documento"></asp:TextBox>
			                        </div>
                                    <div class="form-group">
			                        	<label class="sr-only" for="form-password">Correo Electrónico</label>
			                        	
                                        <asp:TextBox ID="TextCorreoRegistro" name="form-password" placeholder="Correo Electrónico..." class="form-password form-control"  runat="server" ToolTip="Correo Electrónico"></asp:TextBox>
			                        </div>
                                    <div class="form-group">
			                        	<label class="sr-only" for="form-password">Correo Electrónico</label>
			                        	
                                        <asp:TextBox ID="TextConfirmaCorreoRegistro" name="form-password" placeholder="Confirma Correo Electrónico..." class="form-password form-control"  runat="server" ToolTip="Confirma Correo Electrónico"></asp:TextBox>
			                        </div>
                                    <div class="form-group">
			                        	<label class="sr-only" for="form-password">Contraseña</label>
			                        	
                                        <asp:TextBox ID="TextContraseniaRegistro" name="form-password" placeholder="Contraseña..." class="form-password form-control"  runat="server" ToolTip="Contraseña"></asp:TextBox>
			                        </div>
                                    <div class="form-group">
			                        	<label class="sr-only" for="form-password">Domicilio</label>
			                        	
                                        <asp:TextBox ID="TextDomicilioRegistro" name="form-password" placeholder="Domicilio..." class="form-password form-control"  runat="server" ToolTip="Domicilio"></asp:TextBox>
			                        </div>
                                    <div class="form-group">
			                        	<label class="sr-only" for="form-password">Provincia</label>
                                         <!--<asp:DropDownList ID="TextProvinciaRegistro" name="form-password" class="form-password form-control"  runat="server" ToolTip="Provincia"></asp:DropDownList>-->
                                       <asp:TextBox ID="TextProvincia" name="form-password" placeholder="Provincia..." class="form-password form-control"  runat="server" ToolTip="Provincia"></asp:TextBox>
			                        </div>
                                    <div class="form-group">
			                        	<label class="sr-only" for="form-password">Ciudad</label>
                                        <!--<asp:DropDownList ID="TextCiudadRegistros" name="form-password" class="form-password form-control"  runat="server" ToolTip="Domicilio"></asp:DropDownList>-->
                                        <asp:TextBox ID="TextCiudadRegistro" name="form-password" placeholder="Ciudad..." class="form-password form-control"  runat="server" ToolTip="Domicilio"></asp:TextBox>
			                        </div>
			                        
                                    <asp:Button ID="BtnAceptarLogin" CssClass="btnLogin" runat="server" Text="Registrar" />
			                    </form>
		                    </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 social-login">
                        	<h3>...sitios amigos:</h3>
                        	<div class="social-login-buttons">
	                        	<a class="btn btn-link-2" href="https://www.facebook.com/"TARGET="_blank" >
	                        		<i class="fa fa-facebook"></i> Facebook
	                        	</a>
	                        	<a class="btn btn-link-2" href="https://twitter.com/"TARGET="_blank">
	                        		<i class="fa fa-twitter"></i> Twitter
	                        	</a>
	                        	<a class="btn btn-link-2" href="https://plus.google.com/"TARGET="_blank">
	                        		<i class="fa fa-google-plus"></i> Google Plus
	                        	</a>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="assetLogin/assets/js/jquery-1.11.1.min.js"></script>
        <script src="assetLogin/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assetLogin/assets/js/jquery.backstretch.min.js"></script>
        <script src="assetLogin/assets/js/scripts.js"></script>
        
</body>
</html>

