﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="accesodenegado.aspx.cs" Inherits="TPractico2015.accesodenegado" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Acceso Denegado</title>
    <link href="CSS/sistema.css" rel="stylesheet" />
    <link href="CSS/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="divDenegado">
        <asp:Label ID="LblAccesoDenegado" CssClass="LblDenegado" runat="server" Text="ACCESO DENEGADO"></asp:Label>  <br />
        <asp:Button ID="BtnVolverAcceso" runat="server" Text="Volver" CssClass="btn btn-default" OnClick="BtnVolverAcceso_Click" />
    </div>
    </form>
</body>
</html>
