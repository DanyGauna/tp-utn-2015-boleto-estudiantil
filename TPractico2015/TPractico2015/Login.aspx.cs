﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Controladoras;
using Modelos;
using System.Web.Security;

namespace TPractico2015
{
    public partial class Login : System.Web.UI.Page
    {
        CtrlLogin ctrlLogin = new CtrlLogin();
        CtrlInstituciones ctrlInstitucion = new CtrlInstituciones();
        CtrlNivelesLogin ctrlNivelLogin = new CtrlNivelesLogin();
        Usuario usuario = new Usuario();
        Estudiante estudiante = new Estudiante();       

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnAceptarLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    bool existe = true;                   
                    string passCript = Seguridad.Encriptar(TextPasswordLogin.Text);
                    usuario = ctrlLogin.buscarPorNombre(TextUsuarioLogin.Text, passCript);
                    
                    if (usuario != null)
                    {

                        Session["currentUser"] = usuario;
                        Response.Redirect("index.aspx");

                    }
                    else
                    {
                         estudiante = new Estudiante();
                         estudiante = ctrlLogin.buscarPorCorreo(TextUsuarioLogin.Text, passCript);
                         if (estudiante != null)
                         {
                             Session["currentStudent"] = estudiante;
                             Response.Redirect("indexEstudiante.aspx");
                         }
                         else
                         {
                             existe = false;
                         }

                    }
                    if (!existe)
                    {
                        LblErrorLogin.Text = "El usuario o estudiante no existe."; 
                    }
                }
                else
                {
                    LblErrorLogin.Text = "Debe completar los campos.";
                }
            }
            catch (Exception ex)
            {
                LblErrorLogin.Text = ex.Message;
            }

        }

        private bool validaCampos()
        {
            bool rta = true;
            if (TextUsuarioLogin.Text == "")
            {
                rta = false;
            }
            if (TextPasswordLogin.Text == "")
            {
                rta = false;
            }
            return rta;
        }
    }
}