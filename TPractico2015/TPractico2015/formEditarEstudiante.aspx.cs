﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Controladoras;
using Modelos;

namespace TPractico2015
{
    public partial class formEditarEstudiante : System.Web.UI.Page
    {
        CtrlNivelEducativo ctrlNivel = new CtrlNivelEducativo();
        //CtrlUsuarios ctrlUsuarios = new CtrlUsuarios();
        CtrlEstudiantes ctrlEstudiantes = new CtrlEstudiantes();
        CtrlInstituciones ctrlInstituciones = new CtrlInstituciones();
        DateTime fecha;
        Usuario usuario = new Usuario();
        Estudiante estudiante = new Estudiante();

        protected void Page_Load(object sender, EventArgs e)
        {
            usuario = (Usuario)Session["currentUser"];
            if (usuario.Correo == null)
            {
                Response.Redirect("Login.aspx");
            }
            gvAlumnosIngresados.DataSource = ctrlEstudiantes.getAll();
            gvAlumnosIngresados.DataBind();            
            TextNivelEducativoEditEst.DataSource = ctrlNivel.obtenerNivelString();
            TextInstitutoEdu.DataSource = ctrlInstituciones.obtenerInstitucionesString();
            if (!IsPostBack)
            {
                TextInstitutoEdu.DataBind();
                TextNivelEducativoEditEst.DataBind();
            }
            TextIDEditEst.Enabled = false;
            TextFechaAltaEditEst.Enabled = false;

        }

        protected void BtnBuscarEstudiante_Click(object sender, EventArgs e)
        {
            try
            {
                if (TextBuscarEstudiante.Text != "")
                {
                    estudiante = ctrlEstudiantes.buscarPorDocumento(TextBuscarEstudiante.Text);
                    Session["currentStudent"] = estudiante;
                    TextBuscarEstudiante.Text = "";
                    cargarCampos(estudiante);
                }
                else
                {
                    LblError.Text = "Debe ingresar el DNI del estudiante.";
                }
               
            }
            catch (Exception ex)
            {
                LblError.Text = ex.Message;
            }
        }

        private void cargarCampos(Estudiante estudiante)
        {
            TextIDEditEst.Text = Convert.ToString(estudiante.Id);
            TextApellidoEditEst.Text = estudiante.Apellido;
            TextNombreEditEst.Text = estudiante.Nombre;
            TextDocumentoEditEst.Text = estudiante.Documento;
            TextContraseniaEditEst.Text = estudiante.Contrasenia;
            TextTelefonoEditEst.Text = estudiante.Telefono;
            TextNivelEducativoEditEst.SelectedValue = estudiante.NivelEducativo.Nivel;
            TextInstitutoEdu.SelectedValue = estudiante.InstitucionEducativa.Nombre;
            fecha = estudiante.FechaAlta;
            TextCorreo.Text = estudiante.Correo;
            TextCodQR.Text = estudiante.CodigoQR;
            TextFechaAltaEditEst.Text = Convert.ToString(fecha.ToShortDateString());
        }

        protected void BtnAgregarEstudianteEditEst_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {                   

                    cargarEstudiante();
                   ctrlEstudiantes.update(estudiante);
                   limpiarCampos();
                   gvAlumnosIngresados.DataSource = ctrlEstudiantes.getAll();
                   gvAlumnosIngresados.DataBind();

                }
                else
                {
                    LblError.Text = "Debe completar los campos obligatorios";
                }

            }
            catch (Exception ex)
            {
                LblError.Text = ex.Message;
            }
        }

        private void limpiarCampos()
        {
            TextBuscarEstudiante.Text = "";
            TextIDEditEst.Text = "";
            TextApellidoEditEst.Text = "";
            TextNombreEditEst.Text = "";
            TextDocumentoEditEst.Text = "";
            TextContraseniaEditEst.Text = "";
            TextCorreo.Text = "";
            TextCodQR.Text = "";
            TextTelefonoEditEst.Text = "";
            TextFechaAltaEditEst.Text = "";
        }

        private void cargarEstudiante()
        {
            estudiante.Id = Convert.ToInt32(TextIDEditEst.Text);
            estudiante.Apellido = TextApellidoEditEst.Text;
            estudiante.Nombre = TextNombreEditEst.Text;
            estudiante.Documento = TextDocumentoEditEst.Text;
            estudiante.Contrasenia = TextContraseniaEditEst.Text;
            estudiante.Correo = TextCorreo.Text;
            estudiante.CodigoQR = TextCodQR.Text;
            estudiante.Telefono = TextTelefonoEditEst.Text;
            estudiante.NivelEducativo = ctrlNivel.buscarPorNivel(TextNivelEducativoEditEst.SelectedItem.Text);
            estudiante.InstitucionEducativa = ctrlInstituciones.obtenerPorNivel(TextInstitutoEdu.SelectedItem.Text);
            estudiante.FechaAlta = Convert.ToDateTime(TextFechaAltaEditEst.Text);
            
        }

        private bool validaCampos()
        {
            bool rta = true;
            if (TextApellidoEditEst.Text == "")
            {
                rta = false;
            }
            else if (TextNombreEditEst.Text == "")
            {
                rta = false;
            }
            else if (TextDocumentoEditEst.Text == "")
            {
                rta = false;
            }            
            else if (TextTelefonoEditEst.Text == "")
            {
                rta = false;
            }
            else if (TextFechaAltaEditEst.Text == "")
            {
                rta = false;
            }
            return rta;
        }

        protected void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    cargarEstudiante();
                    ctrlEstudiantes.delete(estudiante);
                    limpiarCampos();
                    gvAlumnosIngresados.DataSource = ctrlEstudiantes.getAll();
                    gvAlumnosIngresados.DataBind();
                }
            }
            catch (Exception ex)
            {
                LblError.Text = ex.Message;
            }
        }

        protected void BtnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarCampos();
        }
    }
}