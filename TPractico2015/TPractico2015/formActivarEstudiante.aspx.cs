﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Controladoras;
using Modelos;
using MessagingToolkit.QRCode.Codec;
using MessagingToolkit.QRCode.Codec.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Drawing2D;

namespace TPractico2015
{
    public partial class formActivarEstudiante : System.Web.UI.Page
    {
        CtrlInstituciones ctrlInstituciones = new CtrlInstituciones();
        CtrlEstudiantes ctrlEstudiantes = new CtrlEstudiantes();
        CtrlNivelesLogin ctrlNivelLogin = new CtrlNivelesLogin();
        CtrlUsuarios ctrlUsuarios = new CtrlUsuarios();
        Usuario usuario = new Usuario();
        Estudiante estudiante = new Estudiante();
        string dni;
        

        protected void Page_Load(object sender, EventArgs e)
        {
            usuario = (Usuario)Session["currentUser"];
            if (usuario.Correo == null)
            {
                Response.Redirect("Login.aspx");
            }
            else if (usuario.NivelLogin.Nivel != "Administrador")
            {
                Response.Redirect("accesoDenegado.aspx");

            }

            gvListadeEstudiantes.DataSource = ctrlEstudiantes.getAll();
            gvListadeEstudiantes.DataBind();
            if (!IsPostBack)
            {
                limpiarCampos();
            }
            estudiante = (Estudiante)Session["currentStudent"];

        }

        protected void BtnactivarCuenta_Click(object sender, EventArgs e)
        {
            try
            {
                if (TextDocumento.Text != "")
                {
                    estudiante = ctrlEstudiantes.buscarPorDocumento(TextBuscarEstudiante.Text);
                    if (!estudiante.Activo)
                    {
                        dni = TextBuscarEstudiante.Text;
                        string pass = Seguridad.genera_clave(8);
                        pass = Seguridad.Encriptar(pass);
                        QRCodeEncoder encoder = new QRCodeEncoder();
                        Bitmap img = encoder.Encode(TextDocumento.Text.Trim());
                        MemoryStream ms = new MemoryStream();
                        img.Save(ms, ImageFormat.Jpeg);
                        Borde(ms, Color.White, 4f, dni);
                        //en caso de probarlo en otra pc cambiar la ruta
                        string qrCode = "C:\\Users\\Dany\\Desktop\\UTN2015\\LabIV\\Guia_3\\TPractico2015\\TPractico2015\\Imagenes\\" + dni + ".jpg";
                        //img.Save(qrCode, ImageFormat.Jpeg);
                        ctrlEstudiantes.activarEstudiante(dni, pass, qrCode);
                        ImgQR.ImageUrl = "Imagenes\\" + dni + ".jpg";
                        TextPass.Text = pass;
                        gvListadeEstudiantes.DataSource = ctrlEstudiantes.getAll();
                        gvListadeEstudiantes.DataBind();
                        estudiante = ctrlEstudiantes.buscarPorDocumento(dni);
                        if (enviarCorreo(estudiante, img))
                        {
                            LblError.Text = "Email enviado exitosamente.";
                        }
                        else
                        {
                            LblError.Text = "Error al enviar email.";
                        }

                    }
                    else
                    {
                        LblError.Text = "El estudiante ya está activo!!!.";
                    }

                }
                else
                {
                    LblError.Text = "Debe completar el campor DNI.";
                }
            }
            catch (Exception ex)
            {
                LblError.Text = ex.Message;
            }
            
        }

        private void limpiarCampos()
        {
            TextBuscarEstudiante.Text = "";
            TextNombre.Text = "";
            TextDocumento.Text = "";
            ImgQR.ImageUrl = "Imagenes\\qrcodedefault.png";
            TextPass.Text = "";
            TextPass.Enabled = false;
            LblError.Text = "";


        }
        public static void Borde(Stream imagen, Color color, float grosor, string dni)
        {
            System.Drawing.Image original = System.Drawing.Image.FromStream(imagen);
            System.Drawing.Image imgPhoto = System.Drawing.Image.FromStream(imagen);

            Bitmap bmPhoto = new Bitmap(174, 174, PixelFormat.Format24bppRgb);
            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.SmoothingMode = SmoothingMode.AntiAlias;
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
            grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;

            Rectangle rec = new Rectangle(0, 0, 174, 174);
            grPhoto.DrawImage(imgPhoto, rec);
            Pen pen = new Pen(color, grosor);
            grPhoto.DrawRectangle(pen, rec);

            MemoryStream mm = new MemoryStream();

            bmPhoto.Save("C:\\Users\\Dany\\Desktop\\UTN2015\\LabIV\\Guia_3\\TPractico2015\\TPractico2015\\Imagenes\\" + dni + ".jpg", ImageFormat.Jpeg);
        }

        protected void BtnBuscarEstudiante_Click(object sender, EventArgs e)
        {
            try
            {
                if (TextBuscarEstudiante.Text != "")
                {
                   
                    estudiante = ctrlEstudiantes.buscarPorDocumento(TextBuscarEstudiante.Text);
                    if (estudiante != null)
                    {
                        Session["currentStudent"] = estudiante;
                        cargarCampos(estudiante);
                        gvListadeEstudiantes.DataSource = ctrlEstudiantes.getAll();
                        gvListadeEstudiantes.DataBind();    
                    }
                    else
                    {
                        LblError.Text = "Estudiante no encontrado.";
                    }
                }
                else
                {
                    LblError.Text = "Debe ingresar un DNI";
                }
            }
            catch (Exception ex)
            {
                LblError.Text = ex.Message;
            }
        }

        private void cargarCampos(Estudiante est)
        {
            TextNombre.Text = estudiante.Apellido + " " + estudiante.Nombre;
            TextDocumento.Text = estudiante.Documento;
            TextNombre.Enabled = false;
            TextDocumento.Enabled = false;
            TextPass.Text = estudiante.Contrasenia;
            if (estudiante.CodigoQR != "")
            {
                //Para usar en otra pc cambiar la ruta
                ImgQR.ImageUrl = "Imagenes\\" + estudiante.Documento + ".jpg";
            }
            else
            {
                ImgQR.ImageUrl = "Imagenes\\qrcodedefault.png";
            }
            
        }

        protected void BtnLimpiarCampos_Click(object sender, EventArgs e)
        {
            limpiarCampos();
        }

        private bool enviarCorreo(Estudiante est, Bitmap imagenqr)
        {
            string pass = Seguridad.DesEncriptar(est.Contrasenia);
            string msj = "Bienvenido " + est.Nombre + " " + est.Apellido + " a Boleto Estudiantil. Para ingresar a su cuenta "+
                " deberá loguearse en 'localhost:8117/Login.aspx' con su correo " + est.Correo + " y la contraseña " + pass + ". Su código QR es " + imagenqr;
           bool exito = new EnviarEmail().enviarEmail(est.Correo, msj);
           return exito;
 
        }

        protected void BtnDesactivarCuenta_Click(object sender, EventArgs e)
        {
            try
            {
                if (TextDocumento.Text != "")
                {
                    estudiante = ctrlEstudiantes.buscarPorDocumento(TextDocumento.Text.Trim());
                    if (estudiante.Activo)
                    {
                        ctrlEstudiantes.desactivarEstudiante(TextDocumento.Text);
                        limpiarCampos();
                        gvListadeEstudiantes.DataSource = ctrlEstudiantes.getAll();
                        gvListadeEstudiantes.DataBind();  
                    }
                    else
                    {
                        LblError.Text = "El estudiante ya está inactivo.";
                    }

                    
                }
                else
                {
                    LblError.Text = "Debe completar los campos.";
                }
            }
            catch(Exception ex)
            {
                LblError.Text = ex.Message; 
            }
           
        }
    }
}