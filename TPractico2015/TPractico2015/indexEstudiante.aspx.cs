﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Controladoras;
using Modelos;

namespace TPractico2015
{
    public partial class indexEstudiante : System.Web.UI.Page
    {
        CtrlUrbanTicket ctrlUrbanTicket = new CtrlUrbanTicket();
        CtrlEstudiantes ctrlEstudiante = new CtrlEstudiantes();
        Estudiante estudiante;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            estudiante = (Estudiante)Session["currentStudent"];
            if(estudiante.Correo == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (estudiante.Activo)
            {
                LblEstado.Text = "activo";
            }
            else
            {
                LblEstado.Text = "inactivo"; 
            }
            LblNombreUsuario.Text = estudiante.Apellido +" "+ estudiante.Nombre;
            LblNombreUser.Text = estudiante.Apellido + " " + estudiante.Nombre;
            LblNombreUsuarioSider.Text = estudiante.Apellido + " " + estudiante.Nombre;
            LblActivo.Text = "Activo desde " + ctrlEstudiante.devuelFechaAlta(estudiante.Id).ToShortDateString();
            gvTicketsEstudiantes.DataSource = ctrlUrbanTicket.getPorEstudiante(estudiante.Id);
            gvTicketsEstudiantes.DataBind();
        }
    }
}