﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Controladoras;
using Modelos;

namespace TPractico2015
{
    public partial class formAgregarEstudiante : System.Web.UI.Page
    {
        CtrlNivelEducativo ctrlNivel = new CtrlNivelEducativo();
        //CtrlUsuarios ctrlUsuarios = new CtrlUsuarios();
        CtrlEstudiantes ctrlEstudiantes = new CtrlEstudiantes();
        DateTime fecha;
        Usuario usuario = new Usuario();
        protected void Page_Load(object sender, EventArgs e)
        {
            usuario = (Usuario)Session["currentUser"];
            if (usuario.Correo == null)
            {
                Response.Redirect("Login.aspx");
            }
            TextFechaAlta.Text = Convert.ToString(DateTime.Now.ToShortDateString());
            TextNivelEducativo.DataSource = Session["listaNivelesEducativosString"];
            if (usuario.Institucion != null)
            {
                List<string> listInst = new List<string>();                
                listInst.Add(usuario.Institucion.Nombre);
                TextInstituto.DataSource = listInst;
                TextInstituto.Enabled = false;
            }
            else
            {
                TextInstituto.DataSource = Session["listaInstitutosString"]; 
            }
            
            if (!IsPostBack)
            {
                TextNivelEducativo.DataBind();
                TextInstituto.DataBind();
               
            }
            
            gvAlumnosIngresados.DataSource = Session["listaEstudiantes"];
            gvAlumnosIngresados.DataBind();
            
        }

        protected void BtnAgregarEstudiante_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    
                    fecha = Convert.ToDateTime(TextFechaAlta.Text);
                    if (usuario.Institucion != null)
                    {
                        ctrlEstudiantes.insertar(TextApellidoIngEst.Text, TextNombreIngEst.Text, TextDocumento.Text,TextEmailEst.Text, "", TextTelefono.Text, "", fecha, false, TextNivelEducativo.SelectedItem.Text, usuario.Institucion.Nombre);
                    }
                    else
                    {
                        ctrlEstudiantes.insertar(TextApellidoIngEst.Text, TextNombreIngEst.Text, TextDocumento.Text, TextEmailEst.Text, "", TextTelefono.Text, "", fecha, false, TextNivelEducativo.SelectedItem.Text, TextInstituto.SelectedItem.Text);
                    }
                    gvAlumnosIngresados.DataSource = ctrlEstudiantes.getAllPorInstitucion(usuario.Institucion.Id);
                    gvAlumnosIngresados.DataBind();
                    limpiarCampos();
                }
                else
                {
                    LblErrorAddEst.Text = "Debe completar los campos.";
                }
            }
            catch (Exception ex)
            {
                LblErrorAddEst.Text = ex.Message; 
            }
        }

        private void limpiarCampos()
        {
            TextApellidoIngEst.Text = "";            
            TextNombreIngEst.Text = "";
            TextTelefono.Text = "";
            TextFechaAlta.Text = Convert.ToString(DateTime.Now.ToShortDateString());
            TextDocumento.Text = "";
           // CheckEstado.Checked = false;
            LblErrorAddEst.Text = "";
            TextEmailEst.Text = "";
        }

        private bool validaCampos()
        {
            bool rta = true;
            if (TextNombreIngEst.Text == "")
            {
                rta = false;
            }
            else if (TextApellidoIngEst.Text == "")
            {
                rta = false;
            }
            else if (TextTelefono.Text == "")
            {
                rta = false;
            }
            else if (TextFechaAlta.Text == "")
            {
                rta = false;
            }
            else if (TextDocumento.Text == "")
            {
                rta = false;
            }
            else if (TextEmailEst.Text == "")
            {
                rta = false;
            }
            return rta;
        }
    }
}