﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Controladoras;
using Modelos;

namespace TPractico2015
{
    public partial class formAgregarTransUrbano : System.Web.UI.Page
    {
        CtrlTransportesUrbanos ctrlTransUrbano = new CtrlTransportesUrbanos();
        Usuario usuario;
        protected void Page_Load(object sender, EventArgs e)
        {
            usuario = (Usuario)Session["currentUser"];
            if (usuario.Correo == null)
            {
                Response.Redirect("Login.aspx");
            }
            else if (usuario.NivelLogin.Nivel != "Administrador")
            {
                Response.Redirect("accesoDenegado.aspx");

            }
            gvTransUrbanos.DataSource = Session["listaTransUrbano"];
            gvTransUrbanos.DataBind();
        }

        protected void BtnAgregarTransUrbano_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    ctrlTransUrbano.save(TextDescripcion.Text, TextLinea.Text);
                    gvTransUrbanos.DataSource = ctrlTransUrbano.getAll();
                    gvTransUrbanos.DataBind();
                    limpiarCampos();
                }
                else
                {
                    LblErrorAddTrans.Text = "Debe completar todos los campos";
                }
            }
            catch (Exception ex)
            {
                LblErrorAddTrans.Text = ex.Message;
            }
        }

        private void limpiarCampos()
        {
            TextLinea.Text = "";
            TextDescripcion.Text = "";
        }

        private bool validaCampos()
        {
            bool rta = true;
            if (TextDescripcion.Text == "")
            {
                rta = false;               
            }
            else if (TextLinea.Text == "")
            {
                rta = false;
            }
            return rta;

        }
    }
}