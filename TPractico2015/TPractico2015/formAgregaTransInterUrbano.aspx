﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="formAgregaTransInterUrbano.aspx.cs" Inherits="TPractico2015.formAgregaTransInterUrbano" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <title>Administrador | Ingreso de Transportes InterUrbano</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    
    <link href="assetLogin/plugins/iCheck/all.css" rel="stylesheet" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="wrapper">

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Administrador
            <small>Ingreso de Transportes InterUrbanos</small>
          </h1>         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-4">
              <!-- general form elements -->
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Nuevo Transporte InterUrbano</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Descripción</label>
                      <asp:TextBox ID="TextDescripcionInter" runat="server" class="form-control" placeholder="Descripción..."></asp:TextBox>
                                           
                    </div>                                         
                      <div class="form-group">
                      <label for="exampleInputPassword1">Ruta</label>                     
                          <asp:DropDownList ID="TextRutasUrbanas" runat="server" class="form-control"></asp:DropDownList>
                    </div>                       
                      <div class="form-group">
                      <label for="exampleInputPassword1"></label>                      
                           <asp:Label ID="LblErrorAddInterUrban" runat="server" Text="" CssClass="text-danger"></asp:Label>
                    </div>                                       
                  </div><!-- /.box-body -->
                  <div class="box-footer">                   
                      <asp:Button ID="BtnAgregarTransInterUrbano" class="btn btn-primary" runat="server" Text="Agregar" />
                     
                  </div>
                </form>
              </div><!-- /.box -->              

            </div><!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-8">
              <!-- Horizontal Form -->
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Encargados Ingresados</h3>
                </div><!-- /.box-header -->
                  <asp:GridView ID="gvTransInterUrbanos" runat="server" class="table table-bordered"></asp:GridView>
              </div><!-- /.box -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; 2014-2015 .</strong> Todos los derechos reservados.
      </footer>
      
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
</asp:Content>

