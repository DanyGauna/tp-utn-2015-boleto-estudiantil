﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="TPractico2015.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Inicio</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Inicio           
          </h1>         
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><asp:Label ID="LblCantidadEstudiantes" runat="server" Text=""></asp:Label></h3>
                  <p>Estudiantes</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-people"></i>
                </div>
                <a class="small-box-footer">ADminBUS</a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                   <h3> <asp:Label ID="LblPasajesEmitidos" runat="server" Text=""></asp:Label></h3>
                  <p>Pasajes Emitidos</p>
                </div>
                <div class="icon">
                  <i class="fa fa-ticket"></i>
                </div>
                <a href="#" class="small-box-footer">ADminBUS</a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><asp:Label ID="LblLineasAdd" runat="server" Text=""></asp:Label></h3>
                  <p>Líneas Adheridas</p>
                </div>
                <div class="icon">
                  <i class="fa fa-bus"></i>
                </div>
                <a href="#" class="small-box-footer">ADminBUS</a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><asp:Label ID="LblCantidadUsuarios" runat="server" Text=""></asp:Label></h3>
                  <p>Usuarios</p>
                </div>
                <div class="icon">
                  <i class="fa fa-user"></i>
                </div>
                <a href="#" class="small-box-footer">ADminBUS</a>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->
            <div class="containt">
                <div class="col-lg-4">

                </div>
                <div class="col-lg-4">
                    
                </div>
                <div class="col-lg-4">
                  
                </div>

            </div>
         
    </div><!-- ./wrapper -->
</asp:Content>
