﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Controladoras;
using Modelos;

namespace TPractico2015
{
    public partial class formAgregarEncargado : System.Web.UI.Page
    {

        CtrlInstituciones ctrlInstituciones = new CtrlInstituciones();
        CtrlNivelesLogin ctrlNivelLogin = new CtrlNivelesLogin();
        CtrlUsuarios ctrlUsuarios = new CtrlUsuarios();
        Usuario usuario;

        protected void Page_Load(object sender, EventArgs e)
        {
            usuario = (Usuario)Session["currentUser"];           
            if (usuario.Correo == null)
            {
                Response.Redirect("Login.aspx");
            }
            else if (usuario.NivelLogin.Nivel != "Administrador")
            {
                Response.Redirect("accesoDenegado.aspx");
               
            }
            TextInstitucion.DataSource = Session["listaInstitutosString"];
            if (!IsPostBack)
            {
                TextInstitucion.DataBind();
            }
           
            gvEncargadosIngresados.DataSource = ctrlUsuarios.getAll();
            gvEncargadosIngresados.DataBind();
        }



        private void limpiarCampos()
        {
            TextApellidoEncargado.Text = "";
            TextNombreEncargado.Text = "";
            TextDocumento.Text = "";
            TextCorreo.Text = "";
            TextContrasenia.Text = "";
            TextInstitucion.SelectedIndex = 0;
        }

        private bool validaCampos()
        {
            bool rta = true;

            if (TextApellidoEncargado.Text == "")
            {
                rta = false;
            }
            else if (TextNombreEncargado.Text == "")
            {
                rta = false;
            }
            else if (TextDocumento.Text == "")
            {
                rta = false;
            }
            else if (TextCorreo.Text == "")
            {
                rta = false;
            }
            else if (TextContrasenia.Text == "")
            {
                rta = false;
            }
            else if (TextInstitucion.SelectedItem.Text == "")
            {
                rta = false;
            }

            return rta;

        }

        protected void BtnAgregarEncargado_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    string passCript = Seguridad.Encriptar(TextContrasenia.Text);
                    ctrlUsuarios.insertarUsuario(TextApellidoEncargado.Text, TextNombreEncargado.Text, TextDocumento.Text, TextCorreo.Text, passCript, 2, TextInstitucion.SelectedItem.Text);
                    limpiarCampos();
                    gvEncargadosIngresados.DataSource = ctrlUsuarios.getAll();
                    gvEncargadosIngresados.DataBind();
                }
                else
                {
                    LblErrorAddEncargado.Text = "Debe completar todos los campos.";
                }
            }
            catch (Exception ex)
            {
                LblErrorAddEncargado.Text = ex.Message;
            }
        }

    }
}