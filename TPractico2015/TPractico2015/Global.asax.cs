﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Modelos;

namespace TPractico2015
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {
           Session["currentUser"] = new Usuario();
           Session["currentStudent"] = new Estudiante();
           Session["listaUsuarios"] = new List<Usuario>();
           Session["listaNivelLogin"] = new List<NivelLogin>();
           Session["listaNivelesEducativos"] = new List<NivelEducativo>();
           Session["listaNivelesEducativosString"] = new List<string>();
           Session["listaInstitutos"] = new List<InstitucionEducativa>();
           Session["listaInstitutosString"] = new List<string>();
           Session["listaEstudiantes"] = new List<Estudiante>();
           Session["listaLocalidadesString"] = new List<string>();
           Session["listaProvinciasString"] = new List<string>();
           Session["listaTransUrbano"] = new List<TransporteUrbano>();

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}