﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Controladoras;

namespace TPractico2015
{
    public partial class formNivelesEducativos : System.Web.UI.Page
    {
        CtrlNivelEducativo ctrlNivelEducativo = new CtrlNivelEducativo();
        NivelEducativo nivelEducativo;
        CtrlUtils utils = new CtrlUtils();
        Usuario usuario;
        protected void Page_Load(object sender, EventArgs e)
        {
            usuario = (Usuario)Session["currentUser"];
            if (usuario.Correo == null)
            {
                Response.Redirect("Login.aspx");
            }
            else if (usuario.NivelLogin.Nivel != "Administrador")
            {
                Response.Redirect("accesoDenegado.aspx");

            }
            
            TextProvinciasNivelEducativo.DataSource = Session["listaProvinciasString"];
            if (!IsPostBack)
            {
                TextProvinciasNivelEducativo.DataBind();
            }

            gvNivelesEducativos.DataSource = Session["listaNivelesEducativos"];
            gvNivelesEducativos.DataBind();
        }

        protected void BtnAgregarNivel_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaCampos())
                {
                    ctrlNivelEducativo = new CtrlNivelEducativo();
                    ctrlNivelEducativo.insertar(TextNivel.Text, TextModalidad.Text);
                    gvNivelesEducativos.DataSource = ctrlNivelEducativo.getAll();
                    gvNivelesEducativos.DataBind();

                }
                else
                {
                    LblErrorNV.Text = "Debe completar los campos.";
                }
            }
            catch (Exception ex)
            {
                LblErrorNV.Text = ex.Message;
            }
        }

        private bool validaCampos()
        {
            bool rta = true;
            if (TextProvinciasNivelEducativo.SelectedItem.Text == "")
            {
                rta = false;
            }
            if (TextNivel.Text == "")
            {
                rta = false;
            }
            if (TextModalidad.Text == "")
            {
                rta = false;
            }

            return rta;
        }
    }
}