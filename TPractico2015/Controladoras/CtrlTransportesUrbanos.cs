﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAO;
using Modelos;

namespace Controladoras
{
    public class CtrlTransportesUrbanos
    {
        TransportesUrbanosDAO transUrbanoDAO = TransportesUrbanosDAO.Instancia();

        public void save(string desc, string line)
        {
            try
            {
                TransporteUrbano tpU = transUrbanoDAO.buscarPorLinea(line);
                if (tpU == null)
                {
                    tpU = new TransporteUrbano(desc, line);
                    transUrbanoDAO.save(tpU);
                }
                else
                {
                    throw new Exception("La línea ya existe");
                }
            }
            catch (Exception)
            {
                throw new Exception("Inconvenientes al ingresar un transporte urbano");
            }
        }

        public List<TransporteUrbano> getAll()
        {
            List<TransporteUrbano> lista = transUrbanoDAO.getAll();
            return lista;
        }

        public List<string> lineaString()
        {
            List<string> lista = transUrbanoDAO.lineaString();
            return lista;
        }

        public int cantTotalTransportes()
        {
            int rta = transUrbanoDAO.cantidadTotalTransportes();
            return rta;
        }
    }
}