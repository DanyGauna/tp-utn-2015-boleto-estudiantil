﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAO;
using Modelos;

namespace Controladoras
{
    public class CtrlEstudiantes
    {
        EstudiantesDAO estudiantesDAO = EstudiantesDAO.Instancia();
        CtrlInstituciones ctrlInstituciones = new CtrlInstituciones();
        CtrlNivelEducativo ctrlNivelEducativo = new CtrlNivelEducativo();

        public void insertar(string ape, string nom, string dni, string mail, string pass, string tel, string cod, DateTime fecha, bool est, string nivEdu, string instEdu)
        {
            Estudiante estudiante = new Estudiante();
            estudiante = estudiantesDAO.buscarPorDocumento(dni);

            if (estudiante == null)
            {
                estudiante = estudiantesDAO.buscarPorCorreo(mail);
                if (estudiante == null)
                {
                    NivelEducativo niv = ctrlNivelEducativo.buscarPorNivel(nivEdu);
                    InstitucionEducativa ie = ctrlInstituciones.obtenerPorNivel(instEdu);
                    if (niv != null && ie != null)
                    {
                        estudiante = new Estudiante(ape, nom, dni, mail, pass, tel, cod, fecha, est, niv, ie);
                        estudiantesDAO.save(estudiante);

                    }
                }
                else
                {
                    throw new Exception("Ya hay un estudiante con ese correo.");
                }
                

            }
            else 
            {
                throw new Exception("Ya hay un estudiante con ese número de documento");
            }
        }

        public void update(Estudiante student)
        {
            estudiantesDAO.update(student);
        }
        public void delete(Estudiante student)
        {
            estudiantesDAO.delete(student);
        }

        public Estudiante buscarPorDNIoID(int dato)
        {
            Estudiante estudiante = estudiantesDAO.buscarPorDNIoID(dato);
            return estudiante;
        }

        public void activarEstudiante(string dni, string pass, string qr)
        {
            estudiantesDAO.activarEstudiante(dni, pass, qr);         
        }
        public void desactivarEstudiante(string dni)
        {
            estudiantesDAO.desactivarEstudiante(dni);
        }
        public Estudiante buscarPorDocumento(string dato)
        {
            Estudiante estudiante = estudiantesDAO.buscarPorDocumento(dato);
            return estudiante;
        }

        public DateTime devuelFechaAlta(int dato)
        {
            DateTime fecha = estudiantesDAO.devuelveFechaAlta(dato);
            return fecha;
        }

        public List<Estudiante> getAll()
        {
            List<Estudiante> lista = estudiantesDAO.getAll();
            return lista;
        }
        public List<Estudiante> getAllPorInstitucion(int dato)
        {
            List<Estudiante> lista = estudiantesDAO.getAllPorInstitucion(dato);
            return lista;
        }

        public int cantTotalEstudiantes()
        {
            int total = estudiantesDAO.cantidadTotalEstudiantes();
            return total;
        }
    }
}