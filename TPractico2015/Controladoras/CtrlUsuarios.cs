﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAO;
using Modelos;

namespace Controladoras
{
    public class CtrlUsuarios
    {
        UsuariosDAO usuariosDAO = UsuariosDAO.Instancia();
        CtrlNivelesLogin ctrlNivelLogin = new CtrlNivelesLogin();
        CtrlInstituciones ctrlInstituciones = new CtrlInstituciones();
        public void insertarUsuario(string ape, string nom, string dni, string mail, string pass, int rol, string instEdu)
        {
            Usuario usu = usuariosDAO.buscarPorCorreo(mail);
            if (usu == null)
            {
                InstitucionEducativa insEdu = ctrlInstituciones.obtenerPorNivel(instEdu);
                NivelLogin nivLog = ctrlNivelLogin.getById(rol);
                if (insEdu != null || nivLog != null)
                {
                    usu = new Usuario(ape, nom, dni, mail, pass, nivLog, insEdu);
                    usuariosDAO.save(usu);
                }
                else
                {
                    throw new Exception("Institucion o permiso no encontrado");
                }
            }
            else
            {
                throw new Exception("El usuario ya existe");
            }
        }

        public Usuario obtenerUsuario(string dato)
        {
            Usuario usu = usuariosDAO.buscarPorCorreo(dato);
            return usu;
        }

        public void delete(Usuario dato)
        {
            usuariosDAO.delete(dato); 
        }

        public List<Usuario> getAll()
        {
            List<Usuario> lista = usuariosDAO.getAll();
            return lista;
        }

        public int cantTotalUsuarios()
        {
            int total = usuariosDAO.cantidadTotalUsuarios();
            return total;
        }

        public Usuario buscarPorDocumento(string dato)
        {
            return usuariosDAO.buscarPorDocumento(dato);
        }

    }
}