﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Modelos;
using DAO;

namespace Controladoras
{
    public class CtrlLogin
    {
        UsuariosDAO usuariosDAO = UsuariosDAO.Instancia();
        NivelLoginDAO nvDAO = NivelLoginDAO.Instancia();
        EstudiantesDAO estudianteDAO = EstudiantesDAO.Instancia();

        public Usuario buscarPorNombre(string correo, string pass)
        {
            Usuario login = null;
            login = usuariosDAO.buscarPorCorreo(correo);
            if (login != null)
            {
                if (login.Contrasenia == pass)
                {
                    //login.NivelLogin = nvDAO.getById(login.
                    return login;
                }
                else
                {

                    throw new Exception("La contraseña es incorrecta!!!");
                }
            }
            else
            {
                return login;
            }
        }

        public Estudiante buscarPorCorreo(string correo, string pass)
        {
            Estudiante login = null;
            login = estudianteDAO.buscarPorCorreo(correo);
            if (login != null)
            {
                if (login.Contrasenia == pass)
                {                   
                    return login;
                }
                else
                {

                    throw new Exception("La contraseña es incorrecta!!!");
                }
            }
            else
            {
                throw new Exception("El estudiante no existe!!!");
            }
        }

        public NivelLogin getById(int dato)
        {
            NivelLogin nv = nvDAO.getById(dato);
            return nv;
        }
    }
}