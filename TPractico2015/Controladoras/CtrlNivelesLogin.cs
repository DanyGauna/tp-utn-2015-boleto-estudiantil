﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAO;
using Modelos;

namespace Controladoras
{
    public class CtrlNivelesLogin
    {
        NivelLoginDAO nivelLoginDAO = NivelLoginDAO.Instancia();

        public List<string> obtenerNivelLoginString()
        {
            List<string> lista = nivelLoginDAO.obtenerNivelLoginString();
            return lista;
        }

        public NivelLogin getById(int dato)
        {
            NivelLogin nv = nivelLoginDAO.getById(dato);
            return nv;
        }
    }
}