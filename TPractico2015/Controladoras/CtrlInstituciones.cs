﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAO;
using Modelos;

namespace Controladoras
{
    public class CtrlInstituciones
    {
        InstitucionesDAO institucionesDAO = InstitucionesDAO.Instancia();

        public void save(string nom, List<NivelEducativo> lista)
        {
            InstitucionEducativa institucion = institucionesDAO.buscarPorNivel(nom);
            if (institucion == null)
            {
                InstitucionEducativa inst = new InstitucionEducativa(nom, lista);
                institucionesDAO.save(inst);
            }
        }

        public List<string> obtenerInstitucionesString()
        {
            List<string> lista = institucionesDAO.obtenerInstitucionesString();
            return lista;
        }

        public InstitucionEducativa obtenerPorNivel(string dato)
        {
            InstitucionEducativa ins = institucionesDAO.buscarPorNivel(dato);
            return ins;
        }

        public List<InstitucionEducativa> getAll()
        {
            List<InstitucionEducativa> lista = institucionesDAO.getNivel();
            return lista;
        }
    }
}