﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Modelos;
using DAO;

namespace Controladoras
{
    public class CtrlRutas
    {
        RutasDAO rutasDAO = RutasDAO.Instancia();
        CtrlUtils ctrlUtils = new CtrlUtils();

        public void save(string desde, string hasta, int cantD, DateTime hrSal)
        {
            Localidad des = ctrlUtils.getIdCiudad(desde);
            Localidad has = ctrlUtils.getIdCiudad(hasta);
            if (des != null && has != null)
            {
                Ruta ruta = rutasDAO.existe(des, has);
                if (ruta == null)
                {
                    Frecuencia frec = new Frecuencia(cantD, hrSal);
                    ruta = new Ruta(des, has, frec);
                    rutasDAO.save(ruta);
                }
            }
            else
            {
                throw new Exception("Localidades inválidas");
            }
           
        }
    }
}