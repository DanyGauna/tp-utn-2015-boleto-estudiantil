﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAO;
using Modelos;

namespace Controladoras
{
    public class CtrlNivelEducativo
    {
        NivelEducativoDAO nivelEducativoDAO = NivelEducativoDAO.Instancia();

        public void insertar(string nivel, string modal)
        {       
            NivelEducativo ned = nivelEducativoDAO.buscarPorNivel(nivel);
            if (ned == null)
            {
                NivelEducativo n = new NivelEducativo(nivel, modal);
                nivelEducativoDAO.save(n);
            }
        }

        public List<NivelEducativo> getAll()
        {
            List<NivelEducativo> lista = nivelEducativoDAO.getAll();
            return lista;
        }

        public List<NivelEducativo> getNivel()
        {
            List<NivelEducativo> lista = nivelEducativoDAO.getNivel();
            return lista;
        }
        public List<NivelEducativo> obtenerNivel()
        {
            List<NivelEducativo> lista = nivelEducativoDAO.obtenerNivel();
            return lista;
        }

        public NivelEducativo buscarPorNivel(string dato)
        {
            NivelEducativo nv = nivelEducativoDAO.buscarPorNivel(dato);
            return nv;
        }

        public List<string> obtenerNivelString()
        {
            List<string> lista = nivelEducativoDAO.obtenerNivelString();
            return lista; 
        }
    }
}