﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAO;
using Modelos;

namespace Controladoras
{
    public class CtrlUtils
    {
        Utils utils = new Utils();
        public List<string> obtenerProvincias()
        {
            List<string> listaProv = utils.obtenerProvincias();
            return listaProv;
        }
        public List<string> obtenerLocalidades()
        {
            List<string> listaLoc = utils.obtenerLocalidades();
            return listaLoc;
        }

        public string buscarLocalidadPorId(int dato)
        {
            string loc = utils.buscarLocalidadPorId(dato);
            return loc;
        }

        public Localidad getIdCiudad(string dato)
        {
            Localidad loc = utils.getLocalidad(dato);
            return loc;
        }
    }
}