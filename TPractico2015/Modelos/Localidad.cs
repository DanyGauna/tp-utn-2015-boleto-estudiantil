﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public class Localidad
    {
        int idLocalidad;
        string NombreLocalidad;
        int idProvincia;
        string prefijo;


        public Localidad() { }
        public Localidad(string loc, int idProv, string pref)
        {
            this.NombreLocalidad = loc;
            this.IdProvincia = idProv;
            this.Prefijo = pref;
        }

        public int IdLocalidad
        {
            get { return idLocalidad; }
            set { idLocalidad = value; }
        }

        public string NombreLocalidad1
        {
            get { return NombreLocalidad; }
            set { NombreLocalidad = value; }
        }

        public int IdProvincia
        {
            get { return idProvincia; }
            set { idProvincia = value; }
        }
        

        public string Prefijo
        {
            get { return prefijo; }
            set { prefijo = value; }
        }
    }
}