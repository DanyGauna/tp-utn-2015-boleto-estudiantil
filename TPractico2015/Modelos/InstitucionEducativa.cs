﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public class InstitucionEducativa
    {
        int id;
        string nombre;
        List<NivelEducativo> listaNiveles;

        public InstitucionEducativa() { }

        public InstitucionEducativa(string nom, List<NivelEducativo> list)
        {
            this.Nombre = nom;
            this.ListaNiveles = list;
        }


        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }        

        public List<NivelEducativo> ListaNiveles
        {
            get { return listaNiveles; }
            set { listaNiveles = value; }
        }
    }
}