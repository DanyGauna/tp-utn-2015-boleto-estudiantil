﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public abstract class Ticket
    {
        private int id;
        private DateTime date;
        private Estudiante student;        

        public Ticket(DateTime dt, Estudiante std)
        {
            Date = dt;
            Student = std;
        }

        public Ticket()
        { }


        public int TicketID
        {
            get { return id; }
            set { id = value; }
        }


        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }


        public Estudiante Student
        {
            get { return student; }
            set { student = value; }
        }
    }
}