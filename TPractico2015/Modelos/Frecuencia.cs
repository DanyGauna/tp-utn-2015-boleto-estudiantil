﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public class Frecuencia
    {
        private int id;
        private int cantDiasSemana;
        private DateTime horaSalida;


        public Frecuencia() { }
        public Frecuencia(int cantD, DateTime hrSal)
        {
            this.CantDiasSemana = cantD;
            this.HoraSalida = hrSal;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
       

        public int CantDiasSemana
        {
            get { return cantDiasSemana; }
            set { cantDiasSemana = value; }
        }


        public DateTime HoraSalida
        {
            get { return horaSalida; }
            set { horaSalida = value; }
        }
    }
}