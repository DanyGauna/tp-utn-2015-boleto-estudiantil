﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public class AsientosPlanificados
    {
        private int id;
        private string nroAsiento;
        private bool ocupado;

        public AsientosPlanificados() { }
        public AsientosPlanificados(string num, bool ocup)
        {
            this.NroAsiento = num;
            this.Ocupado = ocup;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        

        public string NroAsiento
        {
            get { return nroAsiento; }
            set { nroAsiento = value; }
        }
       

        public bool Ocupado
        {
            get { return ocupado; }
            set { ocupado = value; }
        }
    }
}