﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public class InterUrbanTicket : Ticket
    {
         //private DateTime hour;
        //public DateTime Hour
        //{
        //    get { return hour; }
        //    set { hour = value; }
        //}

        private TransporteInterUrbano interurbanBus;
        private Ruta ruta;
        private AsientosPlanificados asiento;

        public InterUrbanTicket(DateTime dt, Estudiante std, TransporteInterUrbano ib, Ruta r, AsientosPlanificados s) 
            : base(dt, std)
        {
            InterurbanBus = ib;
            Ruta = r;
            Asiento = s;
            //FechaInterurbano = DateTime.Now;
           // HoraInterurbano = DateTime.Now.Hour; //comentado por vero
        }


        public InterUrbanTicket()
            : base()
        { }



        public TransporteInterUrbano InterurbanBus
        {
            get { return interurbanBus; }
            set { interurbanBus = value; }
        }
        public Ruta Ruta
        {
            get { return ruta; }
            set { ruta = value; }
        }

        public AsientosPlanificados Asiento
        {
            get { return asiento; }
            set { asiento = value; }
        }

    }
}