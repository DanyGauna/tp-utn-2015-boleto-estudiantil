﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public class Provincia
    {
        int idProvincia;
        string nombreProvincia;
        int idPais;

        public string NombreProvincia
        {
            get { return nombreProvincia; }
            set { nombreProvincia = value; }
        }

        public Provincia()
        {
        }

        public Provincia(string prov, int idP)
        {
            this.NombreProvincia = prov;
            this.IdPais = idP;
        }

        public int IdProvincia
        {
            get { return idProvincia; }
            set { idProvincia = value; }
        }

        public int IdPais
        {
            get { return idPais; }
            set { idPais = value; }
        }
    }
}