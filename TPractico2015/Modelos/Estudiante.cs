﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public class Estudiante
    {
        int id;
        string apellido;
        string nombre;
        string documento;
        string correo;
        string contrasenia;
        string telefono;
        string codigoQR;
        DateTime fechaAlta;
        bool activo;
        NivelEducativo nivelEducativo;
        InstitucionEducativa institucionEducativa;

        public Estudiante() { }

        public Estudiante(string ape, string nom, string dni, string mail,string pass, string tel, string cod, DateTime fecha, bool est, NivelEducativo nivEdu, InstitucionEducativa instEdu)
        {
            this.Apellido = ape;
            this.Nombre = nom;
            this.Documento = dni;
            this.Correo = mail;
            this.Contrasenia = pass;
            this.Telefono = tel;
            this.CodigoQR = cod;
            this.FechaAlta = fecha;
            this.Activo = est;
            this.NivelEducativo = nivEdu;
            this.InstitucionEducativa = instEdu;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        
        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
       
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
       
        public string Documento
        {
            get { return documento; }
            set { documento = value; }
        }
       

        public string Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }


        public string Contrasenia
        {
            get { return contrasenia; }
            set { contrasenia = value; }
        }

        public string CodigoQR
        {
            get { return codigoQR; }
            set { codigoQR = value; }
        }
        
        public DateTime FechaAlta
        {
            get { return fechaAlta; }
            set { fechaAlta = value; }
        }
        

        public bool Activo
        {
            get { return activo; }
            set { activo = value; }
        }


        public NivelEducativo NivelEducativo
        {
            get { return nivelEducativo; }
            set { nivelEducativo = value; }
        }


        public InstitucionEducativa InstitucionEducativa
        {
            get { return institucionEducativa; }
            set { institucionEducativa = value; }
        }


        public string Correo
        {
            get { return correo; }
            set { correo = value; }
        }
    }
}