﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public class TransporteUrbano : Transporte
    {
        private string linea;

        public TransporteUrbano(string desc, string l) 
            : base(desc)
        {
            Linea = l;
        }

        public TransporteUrbano() : base() { }


        public string Linea
        {
            get { return linea; }
            set { linea = value; }
        }
    }
}