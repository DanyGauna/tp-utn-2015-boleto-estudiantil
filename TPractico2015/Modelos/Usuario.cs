﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public class Usuario
    {
        int id;
        string apellido;
        string nombre;        
        string correo;
        string contrasenia;
        string documento;
        NivelLogin nivelLogin;
        InstitucionEducativa institucion;

        public Usuario() { }

        public Usuario(string ape, string nom, string dni, string mail, string contrasenia, NivelLogin rol, InstitucionEducativa instEdu)
        {            
            this.Apellido = ape;
            this.Nombre = nom;
            this.Correo = mail;
            this.Contrasenia = contrasenia;
            this.Documento = dni;
            this.NivelLogin = rol;
            this.Institucion = instEdu;

        }
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string Documento
        {
            get { return documento; }
            set { documento = value; }
        }
        public string Correo
        {
            get { return correo; }
            set { correo = value; }
        }
        public string Contrasenia
        {
            get { return contrasenia; }
            set { contrasenia = value; }
        }


        public NivelLogin NivelLogin
        {
            get { return nivelLogin; }
            set { nivelLogin = value; }
        }

        public InstitucionEducativa Institucion
        {
            get { return institucion; }
            set { institucion = value; }
        }
        

        
    }
}