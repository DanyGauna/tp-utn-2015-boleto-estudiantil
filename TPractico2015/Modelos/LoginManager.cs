﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public class LoginManager
    {
        int id;
        string correo;
        string contrasenia;
        int idUsuario;
        int idNivel;


        public LoginManager(int Idd, string c, int idU, int idN)
        {
            this.Id = Idd;
            this.Correo = c;
            this.IdUsuario = idU;
            this.IdNivel = idN;
        }

        public LoginManager()
        {
            // TODO: Complete member initialization
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Correo
        {
            get { return correo; }
            set { correo = value; }
        }

        public string Contrasenia
        {
            get { return contrasenia; }
            set { contrasenia = value; }
        }


        public int IdUsuario
        {
            get { return idUsuario; }
            set { idUsuario = value; }
        }


        public int IdNivel
        {
            get { return idNivel; }
            set { idNivel = value; }
        }


    }
}