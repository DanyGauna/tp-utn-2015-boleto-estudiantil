﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public abstract class Transporte
    {
        private int id;
        private string descripcion;

        public Transporte() { }


        public Transporte(string desc)
        {
            Descripcion = desc;
        }

         public int ID
        {
            get { return id; }
            set { id = value; }
        }
        
        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }
    }
}