﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public class TransporteInterUrbano : Transporte
    {
        private List<Ruta> ruta;
        public TransporteInterUrbano() : base() { }

        public TransporteInterUrbano(string desc, List<Ruta> r)
            : base(desc)
        {
            this.Ruta = r;
        }


        public List<Ruta> Ruta
        {
            get { return ruta; }
            set { ruta = value; }
        }

    }
}