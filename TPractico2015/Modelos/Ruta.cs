﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public class Ruta
    {
        private int id;
        private Localidad desde;
        private Localidad hasta;
        private Frecuencia frecuencia;

        public Ruta(){}

        public Ruta(Localidad des, Localidad has, Frecuencia frec)
        {
            Desde = des;
            Hasta = has;
            frecuencia = frec;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public Localidad Desde
        {
            get { return desde; }
            set { desde = value; }
        }
        public Localidad Hasta
        {
            get { return hasta; }
            set { hasta = value; }
        }
        public Frecuencia Frecuencia
        {
            get { return frecuencia; }
            set { frecuencia = value; }
        }
    }
}