﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public class UrbanTicket : Ticket
    {
        private int internNumber;
        private int authentication;// esto no se que es???
        private TransporteUrbano transporteUrbano;



       

        //ojo que faltan atributos revisar
        public UrbanTicket(DateTime dt, Estudiante std, TransporteUrbano ub, int nro)
            : base(dt, std)
        {
            TransporteUrbano = ub;
            InternNumber = nro;
        }

        public UrbanTicket() : base() { }

        public int InternNumber
        {
            get { return internNumber; }
            set { internNumber = value; }
        }


        public int Authentication
        {
            get { return authentication; }
            set { authentication = value; }
        }


        public TransporteUrbano TransporteUrbano
        {
            get { return transporteUrbano; }
            set { transporteUrbano = value; }
        }
    }
}