﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public class NivelEducativo
    {
        int id;
        string nivel;
        string modalidad;

        public NivelEducativo() { }
        public NivelEducativo(string niv, string mod)
        {
           // this.id = idd;
            this.nivel = niv;
            this.modalidad = mod;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }


        public string Nivel
        {
            get { return nivel; }
            set { nivel = value; }
        }


        public string Modalidad
        {
            get { return modalidad; }
            set { modalidad = value; }
        }
    }
}