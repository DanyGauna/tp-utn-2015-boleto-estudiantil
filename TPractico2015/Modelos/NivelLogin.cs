﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modelos
{
    public class NivelLogin
    {
         int id;
        string nivel;


        public NivelLogin() { }
        public NivelLogin(string desc)
        {
            this.Nivel = desc;
        }


        public int Id
        {
            get { return id; }
            set { id = value; }
        }


        public string Nivel
        {
            get { return nivel; }
            set { nivel = value; }
        }
    }
}