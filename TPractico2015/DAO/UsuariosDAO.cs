﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Modelos;

namespace DAO
{
    public class UsuariosDAO : DAOAbstractoSingleton<UsuariosDAO>, InterfazDAO<Usuario>
    {
        NivelLoginDAO nvDAO = new NivelLoginDAO();
        InstitucionesDAO institucionesDAO = new InstitucionesDAO();
        ConexionBBDD connect = new ConexionBBDD();

        public void save(Usuario dato)
        {
            try
            {
                string cmd = "insert into Usuarios(APELLIDO, NOMBRE,CORREO,CONTRASENIA,DOCUMENTO, ID_NIVELLOGIN, ID_INSTITUCION) values(@APELLIDO, @NOMBRE,@CORREO, @CONTRASENIA,@DOCUMENTO, @ID_NIVELLOGIN, @ID_INSTITUCION)";
                SqlCommand sqlCommand = new SqlCommand(cmd);
                sqlCommand.Parameters.AddWithValue("@APELLIDO", dato.Apellido);
                sqlCommand.Parameters.AddWithValue("@NOMBRE", dato.Nombre);
                sqlCommand.Parameters.AddWithValue("@CORREO", dato.Correo);
                sqlCommand.Parameters.AddWithValue("@CONTRASENIA", dato.Contrasenia);
                sqlCommand.Parameters.AddWithValue("@DOCUMENTO", dato.Documento);
                sqlCommand.Parameters.AddWithValue("@ID_NIVELLOGIN", dato.NivelLogin.Id);
                sqlCommand.Parameters.AddWithValue("@ID_INSTITUCION", dato.Institucion.Id);
                connect.ejecutarSQL(sqlCommand);
              
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Usuario buscarPorDocumento(string dato)
        {
            try
            {
                DataTable dt;
                //List<Usuario> listAux = new List<Usuario>();
                Usuario usuario = null;
                string cmd = "select * from Usuarios where DOCUMENTO = '" + dato + "'";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    Usuario usuarioAux = new Usuario();
                    usuarioAux.Id = (int)aux["ID_USUARIO"];
                    usuarioAux.Apellido = (string)aux["APELLIDO"];
                    usuarioAux.Nombre = (string)aux["NOMBRE"];
                    usuarioAux.Documento = (string)aux["DOCUMENTO"];
                    usuarioAux.Correo = (string)aux["CORREO"];
                    usuarioAux.Contrasenia = (string)aux["CONTRASENIA"];

                    usuario = usuarioAux;
                }
                return usuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void delete(Usuario dato)
        {
            try
            {
                string cmd = "delete Usuarios where ID_USUARIO = " + dato.Id;

                SqlCommand sqlCommand = new SqlCommand(cmd);
                connect.ejecutarSQL(sqlCommand);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void update(Usuario dato)
        {
            throw new NotImplementedException();
        }

        public List<Usuario> getNivel()
        {
            throw new NotImplementedException();
        }

        public List<Usuario> getAll()
        {
            try
            {
                DataTable dt;
                //List<Usuario> listAux = new List<Usuario>();
                List<Usuario> listUsuario = new List<Usuario>();
                string cmd = "select * from Usuarios where ID_INSTITUCION is not null";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {

                    Usuario usu = new Usuario();
                    usu.Id = (int)aux["ID_USUARIO"];
                    usu.Apellido = (string)aux["APELLIDO"];
                    usu.Nombre = (string)aux["NOMBRE"];
                    usu.Correo = (string)aux["CORREO"];
                    usu.Contrasenia = "********";
                    usu.Documento = (string)aux["DOCUMENTO"]; 
                    InstitucionEducativa ie = institucionesDAO.getById((int)aux["ID_INSTITUCION"]);
                    if (ie != null)
                    {
                        usu.Institucion = ie;
                    }
                   
                    listUsuario.Add(usu);
                }
                return listUsuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Usuario getById(int dato)
        {
            throw new NotImplementedException();
        }

        public Usuario buscarPorCorreo(string dato)
        {
            try
            {
                DataTable dt;
                //List<Usuario> listAux = new List<Usuario>();
                Usuario usuario = null;
                string cmd = "select * from Usuarios where correo = '" + dato + "'"; //Select * from BBDDUsuarios where correo ='" + correo + "'";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {

                    Usuario usu = new Usuario();
                    usu.Id = (int)aux["ID_USUARIO"];
                    usu.Apellido = (string)aux["APELLIDO"];
                    usu.Nombre = (string)aux["NOMBRE"];
                    usu.Correo = (string)aux["CORREO"];
                    usu.Contrasenia = (string)aux["CONTRASENIA"];
                    usu.Documento = (string)aux["DOCUMENTO"];                     
                    usu.NivelLogin = nvDAO.getById((int)aux["ID_NIVELLOGIN"]);
                    if (usu.NivelLogin.Id != 1)
                    {
                        usu.Institucion = institucionesDAO.getById((int)aux["ID_INSTITUCION"]); 
                    }                    
                    //listAux.Add(oUsu);
                    usuario = usu;
                }
                return usuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int cantidadTotalUsuarios()
        {
            try
            {
                int rta = 0;
                string cmd = "select COUNT(ID_USUARIO) from Usuarios";
                rta = connect.ejecutarEscalar(cmd);
                return rta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}