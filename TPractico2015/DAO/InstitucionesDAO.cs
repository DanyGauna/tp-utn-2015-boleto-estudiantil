﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Modelos;

namespace DAO
{
    public class InstitucionesDAO : DAOAbstractoSingleton<InstitucionesDAO>, InterfazDAO<InstitucionEducativa>
    {
        ConexionBBDD connect = new ConexionBBDD();
        Utils utils = new Utils();
        public void save(InstitucionEducativa dato)
        {
            try
            {
                string cmd = "insert into InstitucionesEducativas(NOMBRE) values(@NOMBRE)";
                SqlCommand sqlCommand = new SqlCommand(cmd);
                sqlCommand.Parameters.AddWithValue("@NOMBRE", dato.Nombre);
                connect.ejecutarSQL(sqlCommand);
                int idInstitucion = utils.UltimaInstitucion(dato.Nombre);
                for (int i = 0; dato.ListaNiveles.Count > i; i++)
                {
                    string cmdLista = "insert into NivelesPorInstitucion(ID_INSTITUCION, ID_NIVELEDUCATIVO) values(@ID_INSTITUCION,@ID_NIVELEDUCATIVO)";
                    SqlCommand sqlCommandLista = new SqlCommand(cmdLista);
                    sqlCommandLista.Parameters.AddWithValue("@ID_INSTITUCION", idInstitucion);
                    sqlCommandLista.Parameters.AddWithValue("@ID_NIVELEDUCATIVO", dato.ListaNiveles[i].Id);
                    connect.ejecutarSQL(sqlCommandLista);
                }
                
               
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void delete(InstitucionEducativa dato)
        {
            throw new NotImplementedException();
        }

        public void update(InstitucionEducativa dato)
        {
            throw new NotImplementedException();
        }

        public List<InstitucionEducativa> getNivel()
        {
            try
            {
                List<InstitucionEducativa> listIntituciones = new List<InstitucionEducativa>();
                DataTable dt;
                string cmd = "select * from InstitucionesEducativas";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    InstitucionEducativa nivelAux = new InstitucionEducativa();
                    nivelAux.Id = (int)aux["ID_INSTITUCION"];
                    nivelAux.Nombre = (string)aux["NOMBRE"];
                    //listAux.Add(oUsu);
                    listIntituciones.Add(nivelAux);
                }
                return listIntituciones;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> obtenerInstitucionesString()
        {
            try
            {
                List<string> listIntituciones = new List<string>();
                DataTable dt;
                string cmd = "select NOMBRE from InstitucionesEducativas";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    InstitucionEducativa nivelAux = new InstitucionEducativa();                   
                    nivelAux.Nombre = (string)aux["NOMBRE"];                   
                    listIntituciones.Add(nivelAux.Nombre);
                }
                return listIntituciones;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public InstitucionEducativa getById(int dato)
        {
            try
            {
                DataTable dt;
                InstitucionEducativa institucion = new InstitucionEducativa();
                string cmd = "select * from InstitucionesEducativas where ID_INSTITUCION = " + dato;
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    institucion.Id = (int)aux["ID_INSTITUCION"];
                    institucion.Nombre = (string)aux["NOMBRE"];
                }
                return institucion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public InstitucionEducativa buscarPorNivel(string dato)
        {
            try
            {
                DataTable dt;
                //List<Usuario> listAux = new List<Usuario>();
                InstitucionEducativa institucion = null;
                string cmd = "select * from InstitucionesEducativas where NOMBRE = '" + dato + "'";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    InstitucionEducativa institucionAux = new InstitucionEducativa();
                    institucionAux.Id = (int)aux["ID_INSTITUCION"];
                    institucionAux.Nombre = (string)aux["NOMBRE"];
                    institucion = institucionAux;
                }
                return institucion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> listaDeNiveles(int dato)
        {
            try
            {
                List<string> listIntituciones = new List<string>();
                DataTable dt;
                string cmd = "select * from InstitucionesEducativas i "+
                               "inner join NivelesPorInstitucion n on i.ID_INSTITUCION = "+dato+"n.ID_INSTITUCION = "+dato;
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    string idNivel = "";
                    idNivel = (string)aux["ID_NIVELEDUCATIVO"];                   
                    listIntituciones.Add(idNivel+",");
                }
                return listIntituciones;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
