﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAO
{
    public abstract class DAOAbstractoSingleton<S> where S : DAOAbstractoSingleton<S>, new()
    {
        private static S _instancia;

        public static S Instancia()
        {
            if (_instancia == null)
            {
                _instancia = new S();
            }

            return _instancia;
        }
    }
}