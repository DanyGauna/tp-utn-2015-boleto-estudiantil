﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Modelos;
using System.Data;
using System.Data.SqlClient;

namespace DAO
{
    public class Utils
    {
        ConexionBBDD connect = new ConexionBBDD();

        public List<string> obtenerProvincias()
        {          
           
            try
            {
                List<string> lista = new List<string>();
                DataTable dt;
                string cmd = "select PROVINCIA from PROVINCIAS";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    Provincia provinciaAux = new Provincia();                   
                    provinciaAux.NombreProvincia = (string)aux["PROVINCIA"];
                   
                    //listAux.Add(oUsu);
                    lista.Add(provinciaAux.NombreProvincia);
                }
                return lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int buscarProvinciaPorNombre(string dato)
        {
            
            try
            {
                int rta = 0;                 
                string cmd = "select ID_PROVINCIA from Provincias where PROVINCIA = '"+dato+"'";
                rta = connect.ejecutarEscalar(cmd);              
                return rta;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<string> obtenerLocalidades()
        {
            try
            {               
                List<string> lista = new List<string>();
                DataTable dt;
                string cmd = "select DESCRIPCION from Localidades";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    Localidad locAux = new Localidad();
                    locAux.NombreLocalidad1 = (string)aux["DESCRIPCION"];
                    lista.Add(locAux.NombreLocalidad1);
                }
                return lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> obtenerLocalidades(string dato)
        {
            try
            {
                int id = buscarProvinciaPorNombre(dato);
                List<string> lista = new List<string>();
                DataTable dt;
                string cmd = "select DESCRIPCION from Localidades";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    Localidad locAux = new Localidad();
                    locAux.NombreLocalidad1 = (string)aux["DESCRIPCION"];
                    lista.Add(locAux.NombreLocalidad1);
                }
                return lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        public int getIdCiudad(string dato)
        {
            try
            {
                int rta = 0;
                string cmd = "select ID_LOCALIDAD from Localidades where DESCRIPCION = '" + dato + "'";
                rta = connect.ejecutarEscalar(cmd);
                return rta;
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }

        public int UltimaInstitucion(string dato)
        {
            try
            {
                int rta = 0;
                string cmd = "select ID_INSTITUCION from InstitucionesEducativas where NOMBRE = '" + dato + "'";
                rta = connect.ejecutarEscalar(cmd);
                return rta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string buscarProvinciaPorId(int dato)
        {

            try
            {
                DataTable dt;
                string rta = "";
                string cmd = "select PROVINCIA from Provincias where ID_PROVINCIA = '" + dato + "'";
                SqlCommand sqlcmd = new SqlCommand(cmd);
                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    rta = (string)aux["PROVINCIA"];
                }

                return rta;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string buscarLocalidadPorId(int dato)
        {

            try
            {
                DataTable dt;
                string rta = "";
                string cmd = "select DESCRIPCION from Localidades where ID_LOCALIDAD = '" + dato + "'";
                SqlCommand sqlcmd = new SqlCommand(cmd);
                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    rta = (string)aux["DESCRIPCION"];
                }

                return rta;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string buscarTipoDNIPorId(int dato)
        {

            try
            {
                DataTable dt;
                string rta = "";
                string cmd = "select TIPODOCUMENTO from TiposDocumentos where ID_TIPODOCUMENTO = '" + dato + "'";
                SqlCommand sqlcmd = new SqlCommand(cmd);
                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    rta = (string)aux["TIPODOCUMENTO"];
                }

                return rta;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public Localidad getLocalidad(string dato)
        {
            try
            {
                DataTable dt;
                Localidad loc = null;
                string cmd = "select * from Localidades where DESCRIPCION = '" + dato + "'";
                SqlCommand sqlcmd = new SqlCommand(cmd);
                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    Localidad locAux = new Localidad();
                    locAux.IdLocalidad = (int)aux["ID_LOCALIDAD"];
                    locAux.NombreLocalidad1 = (string)aux["DESCRIPCION"];
                    locAux.IdProvincia = (int)aux["ID_PROVINCIA"];
                    locAux.Prefijo = (string)aux["PREFIJO"];
                    loc = locAux;
                }

                return loc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
    }
}