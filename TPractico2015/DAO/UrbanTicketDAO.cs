﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Modelos;
using System.Data;
using System.Data.SqlClient;

namespace DAO
{
    public class UrbanTicketDAO : DAOAbstractoSingleton<UrbanTicketDAO>, InterfazDAO<UrbanTicket>
    {
        TransportesUrbanosDAO tuDAO = new TransportesUrbanosDAO();
        ConexionBBDD connect = new ConexionBBDD();

        public void save(UrbanTicket dato)
        {
            try
            {
                string cmd = "insert into UrbanTicket(FECHA, NUMERO, NUMEROAUTENTICACION, ID_ESTUDIANTE, ID_TRANSURBANO) values(@FECHA, @NUMERO, @NUMEROAUTENTICACION, @ID_ESTUDIANTE, @ID_TRANSURBANO)";
                SqlCommand sqlCommand = new SqlCommand(cmd);
                sqlCommand.Parameters.AddWithValue("@FECHA", dato.Date);
                sqlCommand.Parameters.AddWithValue("@NUMERO", dato.InternNumber);
                sqlCommand.Parameters.AddWithValue("@NUMEROAUTENTICACION", dato.Authentication);
                sqlCommand.Parameters.AddWithValue("@ID_ESTUDIANTE", dato.Student.Id);
                sqlCommand.Parameters.AddWithValue("@ID_TRANSURBANO", dato.TransporteUrbano.ID);

                connect.ejecutarSQL(sqlCommand);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void delete(UrbanTicket dato)
        {
            throw new NotImplementedException();
        }

        public void update(UrbanTicket dato)
        {
            throw new NotImplementedException();
        }

        public List<UrbanTicket> getNivel()
        {
            throw new NotImplementedException();
        }

        public List<UrbanTicket> getPorEstudiante(int dato)
        {
            try
            {
                DataTable dt;
                //List<Usuario> listAux = new List<Usuario>();
                List<UrbanTicket> listTickets = new List<UrbanTicket>();
                string cmd = "select FECHA, NUMERO, NUMEROAUTENTICACION, ID_TRANSURBANO from UrbanTicket where ID_ESTUDIANTE = "+dato;
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    UrbanTicket ticket = new UrbanTicket();
                    ticket.Date = (DateTime)aux["FECHA"];
                    ticket.InternNumber = (int)aux["NUMERO"];
                    ticket.Authentication = (int)aux["NUMEROAUTENTICACION"];
                    ticket.TransporteUrbano = tuDAO.getById((int)aux["ID_TRANSURBANO"]);

                    listTickets.Add(ticket);
                }
                return listTickets;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UrbanTicket> getAll()
        {
            try
            {
                DataTable dt;
                //List<Usuario> listAux = new List<Usuario>();
                List<UrbanTicket> listTickets = new List<UrbanTicket>();
                string cmd = "select FECHA, NUMERO, NUMEROAUTENTICACION, ID_TRANSURBANO from UrbanTicket";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    UrbanTicket ticket = new UrbanTicket();
                    ticket.Date = (DateTime)aux["FECHA"];
                    ticket.InternNumber = (int)aux["NUMERO"];
                    ticket.Authentication = (int)aux["NUMEROAUTENTICACION"];
                    ticket.TransporteUrbano = tuDAO.getById((int)aux["ID_TRANSURBANO"]);
                    

                    listTickets.Add(ticket);
                }
                return listTickets;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public UrbanTicket getById(int dato)
        {
            throw new NotImplementedException();
        }

        public int devuelveCantidad()
        {
            try
            {
                int rta = 0;
                string cmd = "select COUNT(ID_TICKETURBANO) from UrbanTicket";
                rta = connect.ejecutarEscalar(cmd);
                return rta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}