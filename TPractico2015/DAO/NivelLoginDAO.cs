﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Modelos;

namespace DAO
{
    public class NivelLoginDAO : DAOAbstractoSingleton<NivelLoginDAO>, InterfazDAO<NivelLogin>
    {
        ConexionBBDD connect = new ConexionBBDD();

        public void save(NivelLogin dato)
        {
            throw new NotImplementedException();
        }

        public void delete(NivelLogin dato)
        {
            throw new NotImplementedException();
        }

        public void update(NivelLogin dato)
        {
            throw new NotImplementedException();
        }

        public List<NivelLogin> getNivel()
        {
            throw new NotImplementedException();
        }

        public NivelLogin getById(int dato)
        {
            try
            {
                DataTable dt;
                NivelLogin nivel = new NivelLogin();
                string cmd = "select * from NivelLogin where ID = " + dato;
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {                    
                    nivel.Id = (int)aux["ID"];
                    nivel.Nivel = (string)aux["NIVEL"];                   
                }              
                return nivel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public List<string> obtenerNivelLoginString()
        {
            try
            {
                List<string> listNiveles = new List<string>();
                DataTable dt;
                string cmd = "select NIVEL from NivelLogin";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    NivelLogin nivelAux = new NivelLogin();
                    nivelAux.Nivel = (string)aux["NIVEL"];
                    listNiveles.Add(nivelAux.Nivel);
                }
                return listNiveles;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}