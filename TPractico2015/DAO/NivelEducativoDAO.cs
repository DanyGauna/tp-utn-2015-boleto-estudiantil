﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Modelos;

namespace DAO
{
    public class NivelEducativoDAO : DAOAbstractoSingleton<NivelEducativoDAO>, InterfazDAO<NivelEducativo>
    {
        ConexionBBDD connect = new ConexionBBDD();
        public void save(NivelEducativo dato)
        {
           try
            {
                string cmd = "insert into NivelesEducativos(NIVEL, MODALIDAD) values(@NIVEL, @MODALIDAD)";
                SqlCommand sqlCommand = new SqlCommand(cmd);
                sqlCommand.Parameters.AddWithValue("@NIVEL", dato.Nivel);
                sqlCommand.Parameters.AddWithValue("@MODALIDAD", dato.Modalidad);
                connect.ejecutarSQL(sqlCommand);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void delete(NivelEducativo dato)
        {
            throw new NotImplementedException();
        }

        public void update(NivelEducativo dato)
        {
            throw new NotImplementedException();
        }

        public List<NivelEducativo> getAll()
        {
            try
            {
                List<NivelEducativo> listNivel = new List<NivelEducativo>();
                DataTable dt;                
                string cmd = "select * from NivelesEducativos";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    NivelEducativo nivelAux = new NivelEducativo();
                    nivelAux.Id = (int)aux["ID"];
                    nivelAux.Nivel = (string)aux["NIVEL"];
                    nivelAux.Modalidad = (string)aux["MODALIDAD"];
                    //listAux.Add(oUsu);
                    listNivel.Add(nivelAux);
                }
                return listNivel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NivelEducativo getById(int dato)
        {
            try
            {
                DataTable dt;
                NivelEducativo nivel = new NivelEducativo();
                string cmd = "select * from NivelesEducativos where ID = " + dato;
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    nivel.Id = (int)aux["ID"];
                    nivel.Nivel = (string)aux["NIVEL"];
                    nivel.Modalidad = (string)aux["MODALIDAD"];
                }
                return nivel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NivelEducativo buscarPorNivel(string dato)
        {
            try
            {
                DataTable dt;
                //List<Usuario> listAux = new List<Usuario>();
                NivelEducativo nivel = null;
                string cmd = "select * from NivelesEducativos where NIVEL = '" + dato + "'"; 
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    NivelEducativo nivelAux = new NivelEducativo();
                    nivelAux.Id = (int)aux["ID"];
                    nivelAux.Nivel = (string)aux["NIVEL"];
                    nivelAux.Modalidad = (string)aux["MODALIDAD"];
                    //listAux.Add(oUsu);
                    nivel = nivelAux;
                }
                return nivel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<NivelEducativo> getNivel()
        {
            try
            {
                List<NivelEducativo> listNivel = new List<NivelEducativo>();
                DataTable dt;
                string cmd = "select NIVEL from NivelesEducativos";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    NivelEducativo nivelAux = new NivelEducativo();
                    //nivelAux.Id = (int)aux["ID"];
                    nivelAux.Nivel = (string)aux["NIVEL"];
                    //nivelAux.Modalidad = (string)aux["MODALIDAD"];
                    //listAux.Add(oUsu);
                    listNivel.Add(nivelAux);
                }
                return listNivel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<NivelEducativo> obtenerNivel()
        {
            try
            {
                List<NivelEducativo> listNivel = new List<NivelEducativo>();
                DataTable dt;
                string cmd = "select NIVEL from NivelesEducativos";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    NivelEducativo nivelAux = new NivelEducativo();
                    //nivelAux.Id = (int)aux["ID"];
                    nivelAux.Nivel = (string)aux["NIVEL"];
                    //nivelAux.Modalidad = (string)aux["MODALIDAD"];
                    //listAux.Add(oUsu);
                    listNivel.Add(nivelAux);
                }
                return listNivel;
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }

        public List<string> obtenerNivelString()
        {
            try
            {
                List<string> listNivel = new List<string>();
                DataTable dt;
                string cmd = "select NIVEL from NivelesEducativos";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    NivelEducativo nivelAux = new NivelEducativo();
                    //nivelAux.Id = (int)aux["ID"];
                    nivelAux.Nivel = (string)aux["NIVEL"];
                    //nivelAux.Modalidad = (string)aux["MODALIDAD"];
                    //listAux.Add(oUsu);
                    listNivel.Add(nivelAux.Nivel);
                }
                return listNivel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}