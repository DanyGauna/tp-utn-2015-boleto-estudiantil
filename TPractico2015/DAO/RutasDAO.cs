﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Modelos;
using System.Data;
using System.Data.SqlClient;

namespace DAO
{
    public class RutasDAO : DAOAbstractoSingleton<RutasDAO>, InterfazDAO<Ruta>
    {
        ConexionBBDD connect = new ConexionBBDD();
        public void save(Ruta dato)
        {
            try
            {
                string cmdF = "inser into Frecuencias(CANTIDADDEDIAS, HORARIOSALIDA) values(@CANTIDADDEDIAS, @HORARIOSALIDA)";
                SqlCommand sqlCommandF = new SqlCommand(cmdF);
                sqlCommandF.Parameters.AddWithValue("@CANTIDADDEDIAS", dato.Frecuencia.CantDiasSemana);
                sqlCommandF.Parameters.AddWithValue("@HORARIOSALIDA", dato.Frecuencia.HoraSalida);


                int i = connect.ejecutarEscalar("@@IDENTITY");

                string cmd = "insert into Rutas(ID_LOCALIDADDESDE, ID_LOCALIDADHASTA, ID_FRECUENCIA) values(@ID_LOCALIDADDESDE, @ID_LOCALIDADHASTA, @FRECUENCIA)";
                SqlCommand sqlCommand = new SqlCommand(cmd);
                sqlCommand.Parameters.AddWithValue("@ID_LOCALIDADDESDE", dato.Desde.IdLocalidad);
                sqlCommand.Parameters.AddWithValue("@ID_LOCALIDADHASTA", dato.Hasta.IdLocalidad);
                sqlCommand.Parameters.AddWithValue("@ID_FRECUENCIA", i);

                connect.ejecutarSQL(sqlCommand);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void delete(Ruta dato)
        {
            throw new NotImplementedException();
        }

        public void update(Ruta dato)
        {
            throw new NotImplementedException();
        }

        public List<Ruta> getNivel()
        {
            throw new NotImplementedException();
        }

        public Ruta getById(int dato)
        {
            throw new NotImplementedException();
        }

        public Ruta existe(Localidad desde, Localidad hasta)
        {
            try
            {
                DataTable dt;
                //List<Usuario> listAux = new List<Usuario>();
                Ruta ruta = null;
                string cmd = "select * from Rutas where ID_LOCALIDADDESDE = " + desde.IdLocalidad + " and ID_LOCALIDADHASTA = "+hasta.IdLocalidad;
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    Ruta rutaAux = new Ruta();
                    rutaAux.Id = (int)aux["ID_RUTA"];
                    rutaAux.Desde.IdLocalidad = (int)aux["ID_LOCALIDADDESDE"];
                    rutaAux.Hasta.IdLocalidad = (int)aux["ID_LOCALIDADHASTA"];
                    rutaAux.Frecuencia.Id = (int)aux["ID_FRECUENCIA"];
                    ruta = rutaAux;
                }
                return ruta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}