﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Modelos;

namespace DAO
{
    public class EstudiantesDAO : DAOAbstractoSingleton<EstudiantesDAO>, InterfazDAO<Estudiante>
    {
        InstitucionesDAO institucionesDAO = new InstitucionesDAO();
        NivelEducativoDAO nivelDAO = new NivelEducativoDAO();
        ConexionBBDD connect = new ConexionBBDD();
        public void save(Estudiante dato)
        {
            try
            {
                string cmd = "insert into Estudiantes(APELLIDO, NOMBRE, DOCUMENTO, CORREO, CONTRASENIA, TELEFONO, CODIGOQR, FECHADEALTA, ACTIVO, ID_NIVELEDUCATIVO, ID_INSTITUTOEDUCATIVO) values(@APELLIDO, @NOMBRE, @DOCUMENTO,@CORREO, @CONTRASENIA, @TELEFONO, @CODIGOQR, @FECHADEALTA, @ACTIVO, @ID_NIVELEDUCATIVO, @ID_INSTITUTOEDUCATIVO)";
                SqlCommand sqlCommand = new SqlCommand(cmd);
                sqlCommand.Parameters.AddWithValue("@APELLIDO", dato.Apellido);
                sqlCommand.Parameters.AddWithValue("@NOMBRE", dato.Nombre);
                sqlCommand.Parameters.AddWithValue("@DOCUMENTO", dato.Documento);
                sqlCommand.Parameters.AddWithValue("@CORREO", dato.Correo);
                sqlCommand.Parameters.AddWithValue("@CONTRASENIA", dato.Contrasenia);
                sqlCommand.Parameters.AddWithValue("@TELEFONO", dato.Telefono);
                sqlCommand.Parameters.AddWithValue("@CODIGOQR", dato.CodigoQR);
                sqlCommand.Parameters.AddWithValue("@FECHADEALTA", dato.FechaAlta);
                sqlCommand.Parameters.AddWithValue("@ACTIVO", dato.Activo);
                sqlCommand.Parameters.AddWithValue("@ID_NIVELEDUCATIVO", dato.NivelEducativo.Id);
                sqlCommand.Parameters.AddWithValue("@ID_INSTITUTOEDUCATIVO", dato.InstitucionEducativa.Id);

                connect.ejecutarSQL(sqlCommand);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void delete(Estudiante dato)
        {
            try
            {
                string cmd = "delete Estudiantes where ID_ESTUDIANTE = "+dato.Id;

                SqlCommand sqlCommand = new SqlCommand(cmd);
                connect.ejecutarSQL(sqlCommand);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void update(Estudiante dato)
        {
            try
            {                
                string cmd = "update Estudiantes set APELLIDO = '" + dato.Apellido + "', NOMBRE = '" + dato.Nombre + "',"
                    + "  DOCUMENTO = '" + dato.Documento + "', TELEFONO = '" + dato.Telefono + "', CORREO = '"+dato.Correo+"', CODIGOQR = '" + dato.CodigoQR + "'"
                    + ",  ID_NIVELEDUCATIVO  = " + dato.NivelEducativo.Id
                    + ", ID_INSTITUTOEDUCATIVO = " + dato.InstitucionEducativa.Id + ", CONTRASENIA = '" + dato.Contrasenia +"' where ID_ESTUDIANTE = "+dato.Id;
                
                SqlCommand sqlCommand = new SqlCommand(cmd);
                connect.ejecutarSQL(sqlCommand);
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }

        public void activarEstudiante(string dni, string pass, string qr)
        {
            try
            {
                string cmd = "update Estudiantes set CONTRASENIA = '"+ pass + "', CODIGOQR = '"+ qr + "', ACTIVO = "+1+" where DOCUMENTO = '"+dni+"'";
                SqlCommand sqlCommand = new SqlCommand(cmd);
                connect.ejecutarSQL(sqlCommand);
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }
        public void desactivarEstudiante(string dni)
        {
            try
            {
                string cmd = "update Estudiantes set ACTIVO = " + 0 + " where DOCUMENTO = '" + dni + "'";
                SqlCommand sqlCommand = new SqlCommand(cmd);
                connect.ejecutarSQL(sqlCommand);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Estudiante> getNivel()
        {
            throw new NotImplementedException();
        }

        public List<Estudiante> getAllPorInstitucion(int dato)
        {
            try
            {
                DataTable dt;
                //List<Usuario> listAux = new List<Usuario>();
                List<Estudiante> listUsuario = new List<Estudiante>();
                string cmd = "select * from Estudiantes where ID_INSTITUTOEDUCATIVO = "+dato;
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {

                    Estudiante estudiante = new Estudiante();
                    estudiante.Id = (int)aux["ID_ESTUDIANTE"];
                    estudiante.Apellido = (string)aux["APELLIDO"];
                    estudiante.Nombre = (string)aux["NOMBRE"];
                    estudiante.Documento = (string)aux["DOCUMENTO"];
                    estudiante.Correo = (string)aux["CORREO"];
                    estudiante.Telefono = (string)aux["TELEFONO"];
                    estudiante.CodigoQR = (string)aux["CODIGOQR"];
                    estudiante.FechaAlta = (DateTime)aux["FECHADEALTA"];
                    estudiante.Activo = (bool)aux["ACTIVO"];
                    // estudiante.NivelEducativo.Id = (int)aux["ID_NIVELEDUCATIVO"];
                    // estudiante.InstitucionEducativa.Id = (int)aux["ID_INSTITUTOEDUCATIVO"];
                    //string pass = Seguridad.DesEncriptar((string)aux["CONTRASENIA"]);
                    estudiante.Contrasenia = (string)aux["CONTRASENIA"];

                    listUsuario.Add(estudiante);
                }
                return listUsuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Estudiante> getAll()
        {
            try
            {
                DataTable dt;
                //List<Usuario> listAux = new List<Usuario>();
                List<Estudiante> listUsuario = new List<Estudiante>();
                string cmd = "select * from Estudiantes ";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {

                    Estudiante estudiante = new Estudiante();
                    estudiante.Id = (int)aux["ID_ESTUDIANTE"];
                    estudiante.Apellido = (string)aux["APELLIDO"];
                    estudiante.Nombre = (string)aux["NOMBRE"];
                    estudiante.Documento = (string)aux["DOCUMENTO"];
                    estudiante.Correo = (string)aux["CORREO"];
                    estudiante.Telefono = (string)aux["TELEFONO"];
                    estudiante.CodigoQR = (string)aux["CODIGOQR"];
                    estudiante.FechaAlta = (DateTime)aux["FECHADEALTA"];
                    estudiante.Activo = (bool)aux["ACTIVO"];
                   // estudiante.NivelEducativo.Id = (int)aux["ID_NIVELEDUCATIVO"];
                   // estudiante.InstitucionEducativa.Id = (int)aux["ID_INSTITUTOEDUCATIVO"];
                    //string pass = Seguridad.DesEncriptar((string)aux["CONTRASENIA"]);
                    estudiante.Contrasenia = (string)aux["CONTRASENIA"];
                    
                    listUsuario.Add(estudiante);
                }
                return listUsuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Estudiante getById(int dato)
        {
            throw new NotImplementedException();
        }

        public Estudiante buscarPorCorreo(string dato)
        {
            try
            {
                DataTable dt;               
                Estudiante estudiante = null;
                string cmd = "select * from Estudiantes where correo = '" + dato + "'";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    Estudiante est = new Estudiante();
                    est.Id = (int)aux["ID_ESTUDIANTE"];
                    est.Apellido = (string)aux["APELLIDO"];
                    est.Nombre = (string)aux["NOMBRE"];
                    est.Correo = (string)aux["CORREO"];
                    est.Contrasenia = (string)aux["CONTRASENIA"];
                    est.CodigoQR = (string)aux["CODIGOQR"];
                    est.Documento = (string)aux["DOCUMENTO"];                    
                    estudiante = est;
                }
                return estudiante;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Estudiante buscarPorDocumento(string dato)
        {
            try
            {
                DataTable dt;
                //List<Usuario> listAux = new List<Usuario>();
                Estudiante estudiante = null;
                string cmd = "select * from Estudiantes where DOCUMENTO = '" + dato + "'";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    Estudiante estudianteAux = new Estudiante();
                    estudianteAux.Id = (int)aux["ID_ESTUDIANTE"];
                    estudianteAux.Apellido = (string)aux["APELLIDO"];
                    estudianteAux.Nombre = (string)aux["NOMBRE"];
                    estudianteAux.Documento = (string)aux["DOCUMENTO"];
                    estudianteAux.Correo = (string)aux["CORREO"];
                    estudianteAux.Contrasenia = (string)aux["CONTRASENIA"];
                    estudianteAux.Telefono = (string)aux["TELEFONO"];
                    estudianteAux.CodigoQR = (string)aux["CODIGOQR"];
                    estudianteAux.FechaAlta = (DateTime)aux["FECHADEALTA"];
                    estudianteAux.Activo = (bool)aux["ACTIVO"];
                    estudianteAux.InstitucionEducativa = institucionesDAO.getById((int)aux["ID_INSTITUTOEDUCATIVO"]);
                    estudianteAux.NivelEducativo = nivelDAO.getById((int)aux["ID_NIVELEDUCATIVO"]);

                    estudiante = estudianteAux;
                }
                return estudiante;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Estudiante buscarPorDNIoID(int dato)
        {
            try
            {
                DataTable dt;
                //List<Usuario> listAux = new List<Usuario>();
                Estudiante estudiante = null;
                string cmd = "select * from Estudiantes where DOCUMENTO = '" + dato + "' or ID_ESTUDIANTE = "+dato;
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    Estudiante estudianteAux = new Estudiante();
                    estudianteAux.Id = (int)aux["ID_ESTUDIANTE"];
                    estudianteAux.Apellido = (string)aux["APELLIDO"];
                    estudianteAux.Nombre = (string)aux["NOMBRE"];
                    estudianteAux.Documento = (string)aux["DOCUMENTO"];
                    estudianteAux.Contrasenia = (string)aux["CONTRASENIA"];
                    estudianteAux.Telefono = (string)aux["TELEFONO"];
                    estudianteAux.CodigoQR = (string)aux["CODIGOQR"];
                    estudianteAux.FechaAlta = (DateTime)aux["FECHADEALTA"];
                    int idie = (int)aux["ID_INSTITUTOEDUCATIVO"];
                    estudianteAux.InstitucionEducativa = institucionesDAO.getById(idie);
                    int idne = (int)aux["ID_NIVELEDUCATIVO"];
                    estudianteAux.NivelEducativo = nivelDAO.getById(idne);
                    estudianteAux.Activo = (bool)aux["ACTIVO"];

                    estudiante = estudianteAux;
                }
                return estudiante;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DateTime devuelveFechaAlta(int dato)
        {
            try
            {
                DataTable dt;
                //List<Usuario> listAux = new List<Usuario>();
                Estudiante estudiante = null;
                string cmd = "select FECHADEALTA from Estudiantes where ID_ESTUDIANTE = '" + dato + "'";
                SqlCommand sqlcmd = new SqlCommand(cmd);
                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    Estudiante estudianteAux = new Estudiante();                    
                    estudianteAux.FechaAlta = (DateTime)aux["FECHADEALTA"];
                    estudiante = estudianteAux;
                }
                return estudiante.FechaAlta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int cantidadTotalEstudiantes()
        {
            try
            {
                int rta = 0;
                string cmd = "select COUNT(ID_ESTUDIANTE) from Estudiantes";
                rta = connect.ejecutarEscalar(cmd);
                return rta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}