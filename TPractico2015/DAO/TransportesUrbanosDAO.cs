﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Modelos;
using System.Data;
using System.Data.SqlClient;

namespace DAO
{
    public class TransportesUrbanosDAO : DAOAbstractoSingleton<TransportesUrbanosDAO>, InterfazDAO<TransporteUrbano>
    {
        ConexionBBDD connect = new ConexionBBDD();

        public void save(TransporteUrbano dato)
        {
            try
            {
                string cmd = "insert into TransporteUrbano(DESCRIPCION, LINEA) values(@DESCRIPCION, @LINEA)";
                SqlCommand sqlCommand = new SqlCommand(cmd);
                sqlCommand.Parameters.AddWithValue("@DESCRIPCION", dato.Descripcion);
                sqlCommand.Parameters.AddWithValue("@LINEA", dato.Linea);
                connect.ejecutarSQL(sqlCommand);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void delete(TransporteUrbano dato)
        {
            throw new NotImplementedException();
        }

        public void update(TransporteUrbano dato)
        {
            throw new NotImplementedException();
        }

        public List<TransporteUrbano> getAll()
        {
            try
            {
                DataTable dt;
                //List<Usuario> listAux = new List<Usuario>();
                List<TransporteUrbano> listTrans = new List<TransporteUrbano>();
                string cmd = "select * from TransporteUrbano";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {
                    TransporteUrbano trans = new TransporteUrbano();
                    trans.ID = (int)aux["ID_TRANSURBANO"];
                    trans.Descripcion = (string)aux["DESCRIPCION"];
                    trans.Linea = (string)aux["LINEA"];

                    listTrans.Add(trans);
                }
                return listTrans;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TransporteUrbano> getNivel()
        {
            throw new NotImplementedException();
        }

        public TransporteUrbano getById(int dato)
        {
            try
            {
                DataTable dt;
                TransporteUrbano tu = new TransporteUrbano();
                string cmd = "select LINEA from TransporteUrbano where ID_TRANSURBANO = " + dato;
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {                   
                    tu.Linea = (string)aux["LINEA"];
                }
                return tu;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TransporteUrbano buscarPorLinea(string dato)
        {
            try
            {
                DataTable dt;
                //List<Usuario> listAux = new List<Usuario>();
                TransporteUrbano trUr = null;
                string cmd = "select * from TransporteUrbano where LINEA = '" + dato + "'";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {

                    TransporteUrbano trUrAux = new TransporteUrbano();
                    trUrAux.ID = (int)aux["ID_TRANSURBANO"];
                    trUrAux.Descripcion = (string)aux["DESCRIPCION"];
                    trUrAux.Linea = (string)aux["LINEA"];

                    trUr = trUrAux;
                }
                return trUr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> lineaString()
        {
            try
            {
                DataTable dt;
                List<string> lista = new List<string>();
                string cmd = "select LINEA from TransporteUrbano";
                SqlCommand sqlcmd = new SqlCommand(cmd);

                dt = connect.cargarDatos(sqlcmd);
                foreach (DataRow aux in dt.Rows)
                {lista.Add((string)aux["LINEA"]);
                }
                return lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int cantidadTotalTransportes()
        {
            try
            {
                int rta = 0;
                string cmd = "select COUNT(ID_TRANSURBANO) from TransporteUrbano";
                rta = connect.ejecutarEscalar(cmd);
                return rta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}