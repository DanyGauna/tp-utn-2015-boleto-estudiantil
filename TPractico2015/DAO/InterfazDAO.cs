﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAO
{
    public interface InterfazDAO<T>
    {
        void save(T dato);
        void delete(T dato);
        void update(T dato);
        List<T> getNivel();
        T getById(int dato);

    }
}