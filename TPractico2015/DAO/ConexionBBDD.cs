﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Modelos;
using System.Threading.Tasks;
using System.Configuration;

namespace DAO
{
    public class ConexionBBDD : DAOAbstractoSingleton<ConexionBBDD>
    {
       // public SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["StringDeConexion"].ConnectionString);
       // static SqlConnection cn = new SqlConnection("Data Source=LAPCDEDANY\\SQLEXPRESS;Initial Catalog=TP2015;Integrated Security=True");
        public SqlConnection cn = new SqlConnection("Data Source=LAPCDEDANY\\SQLEXPRESS;Initial Catalog=TP2015;Integrated Security=True;");

        public void conectar()
        {
            try
            {
                if (cn.State == ConnectionState.Closed)
                {
                    cn.Open();
                }
            }
            catch (Exception)
            {
                throw new Exception("Error de conexion");
            }
        }
        public void desconectar()
        {
            try
            {
                cn.Close();
            }
            catch (Exception)
            {
                throw new Exception("Error de desconexion");
            }
        }
        public void ejecutarSQL(SqlCommand cmd)
        {
            try
            {
                // SqlCommand cmd = new SqlCommand(cmdText, cn);
                cmd.Connection = cn;
                conectar();
                cmd.ExecuteNonQuery();
                desconectar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /*static public DataTable cargarDatos(string cmdText)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand(cmdText, cn);
                conectar();
                SqlDataReader reader = cmd.ExecuteReader();
                dt.Load(reader);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }*/
        //Nuevo cargar datos
        public DataTable cargarDatos(SqlCommand cmd)
        {
            try
            {
                DataTable dt = new DataTable();
                cmd.Connection = cn;
                conectar();
                //cmd.Prepare();
                SqlDataReader reader = cmd.ExecuteReader();
                dt.Load(reader);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int ejecutarEscalar(string cmdText)
        {
            try
            {
                conectar();
                SqlCommand cmd = new SqlCommand(cmdText, cn);
                int aux = Convert.ToInt32(cmd.ExecuteScalar());
                desconectar();
                return aux;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public SqlDataReader TraerDatosEnReader(SqlCommand sqlcmd)
        {
            try
            {
                sqlcmd.Connection = cn;
                conectar();
                SqlDataReader reader = sqlcmd.ExecuteReader();
                return reader;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //desconectar();
            }
        }
    }
}